/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.utils

import org.scalatest.{Matchers, WordSpecLike}

class TestMapUtils extends WordSpecLike with Matchers {
  /*
  "MapUtils tripletToMap" must {
    "return empty Map" in {
      MapUtils.tripletToMap(List(("", "", ""))) shouldBe Map("" -> Map("" -> ""))
    }

    "return correct Map" in {
      MapUtils.tripletToMap(List(("m.login.dummy", "param1", "value1"), ("m.login.dummy", "param2", "value2"))) shouldBe Map(
        "m.login.dummy" -> Map("param1" -> "value1", "param2" -> "value2"))
    }
    "return correct Map too" in {
      val ret = MapUtils.tripletToMap(
        List(("m.login.recaptcha", "param1", "value1"),
             ("m.login.dummy", "param1", "value1"),
             ("m.login.dummy", "param2", "value2"))) shouldBe Map("m.login.dummy" -> Map("param1" -> "value1",
                                                                                         "param2" -> "value2"),
                                                                  "m.login.recaptcha" -> Map("param1" -> "value1"))
    }
  }

 */
}
