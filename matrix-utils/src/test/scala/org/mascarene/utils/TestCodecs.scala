package org.mascarene.utils

import org.scalatest.{Matchers, WordSpecLike}

class TestCodecs extends WordSpecLike with Matchers {
  "Codecs base64 encoder/decoder" must {
    "provide same string when encode/decode" in {
      val testString = "SomeTestString"
      Codecs.base64Decode(Codecs.base64Encode(testString)) shouldBe testString
    }
  }
}
