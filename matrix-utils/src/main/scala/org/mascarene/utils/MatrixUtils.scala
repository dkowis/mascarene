/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.utils

import scala.util.{Failure, Success, Try}

sealed trait Sigil
case object UserSigil      extends Sigil { override def toString: String = "@" }
case object RoomSigil      extends Sigil { override def toString: String = "!" }
case object EventSigil     extends Sigil { override def toString: String = "$" }
case object RoomAliasSigil extends Sigil { override def toString: String = "#" }

trait MatrixIdentifier {
  def localPart: String
  def domain: String
  def sigil: Sigil
  override def toString: String = s"$sigil$localPart:$domain"
}

case class UserIdentifier(localPart: String, domain: String) extends MatrixIdentifier {
  val sigil: Sigil     = UserSigil
  def isValid: Boolean = UserIdentifierUtils.parse(this.toString).isSuccess
}

case class RoomIdentifier(localPart: String, domain: String) extends MatrixIdentifier {
  val sigil: Sigil     = RoomSigil
  def isValid: Boolean = RoomIdentifierUtils.parse(this.toString).isSuccess
}

object UserIdentifierUtils {
  private val userIdPattern = """^@([\p{Lower}\d\._=-]*):(.+)""".r
  def parse(identifier: String): Try[UserIdentifier] = {
    identifier match {
      case userIdPattern(localName, domain) => Success(UserIdentifier(localName, domain))
      case _                                => Failure(new IllegalArgumentException(s"'$identifier' doesn't match matrix User ID format"))
    }
  }
  def build(localPart: String, domainName: String): Try[UserIdentifier] = {
    val built = UserIdentifier(localPart, domainName)
    if (built.isValid)
      Success(built)
    else
      Failure(new IllegalAccessException(s"'$built' doesn't match matrix User ID format"))
  }

  def generate(domainName: String): UserIdentifier = UserIdentifier(Codecs.genUserId(), domainName)
}

object RoomIdentifierUtils {
  private val roomIdPattern = """^!([\p{Alnum}\._=-]*):(.+)""".r
  def parse(identifier: String): Try[RoomIdentifier] = {
    identifier match {
      case roomIdPattern(localName, domain) => Success(RoomIdentifier(localName, domain))
      case _                                => Failure(new IllegalArgumentException(s"'$identifier' doesn't match matrix Room ID format"))
    }
  }
  def build(localPart: String, domainName: String): Try[RoomIdentifier] = {
    val built = RoomIdentifier(localPart, domainName)
    if (built.isValid)
      Success(built)
    else
      Failure(new IllegalAccessException(s"'$built' doesn't match matrix Room ID format"))
  }

  def generate(domainName: String): RoomIdentifier = RoomIdentifier(Codecs.genRoomId(), domainName)
}
