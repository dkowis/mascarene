package org.mascarene.utils

import java.time.{LocalDateTime, ZoneOffset}

object DateTimeUtils {
  def nowMilis: Long = {
    val now = LocalDateTime.now(ZoneOffset.UTC)
    now.atZone(ZoneOffset.UTC).toInstant.toEpochMilli
  }
}
