/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.utils

object MapUtils {
  /*
  def tripletToMap[T](triplet: List[(T, T, T)]): Map[T, Map[T, T]] =
    triplet
      .groupBy(t => t._1)
      .view
      .mapValues(v => v.map(vv => (vv._2, vv._3)).groupBy(t => t._1).view.mapValues(vvv => vvv.map(_._2).head))
  def tripletToMap[T](triplet: List[(T, T, T)]): Map[T, Map[T, T]] =
    triplet
      .groupMap(t => t._1)(vv => (vv._2, vv._3))
      .groupMap(t => t._1)(vvv => vvv._2.head)

 */
}
