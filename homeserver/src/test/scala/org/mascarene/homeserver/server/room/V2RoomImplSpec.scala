/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.room

import java.time.LocalDateTime
import java.util.UUID

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.mascarene.homeserver.server.model.{AuthRepo, EventRepo, EventTypes, Room, RoomRepo, User}
import org.mascarene.homeserver.server.rooms.{PowerLevelsUtils, StateSetCache, V2RoomImpl}
import org.mascarene.matrix.client.r0.model.rooms.{CreationContent, PowerLevelEventContent}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.TestEnv
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.mascarene.utils.Codecs
import org.scalatest.BeforeAndAfterAll

import scala.util.{Failure, Success}

class V2RoomImplSpec extends AnyWordSpecLike with Matchers with FailFastCirceSupport with TestEnv {

  private[this] val eventRepo     = new EventRepo(dbContext)
  private[this] val authRepo      = new AuthRepo(dbContext)
  private[this] val roomRepo      = new RoomRepo(dbContext)
  private[this] val stateSetCache = new StateSetCache(config, dbContext, jedisPool)

  flyway.clean()
  flyway.migrate()

  var sequence = 0L

  private def insertCreateEvent(mxEventId: String, room: Room, sender: User) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_CREATE,
        Some(""),
        Some(CreationContent(creator = sender.mxUserId, room_version = Some("5")).asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = LocalDateTime.now(),
        streamOrder = sequence
      )
      .get
  }

  private def insertMemberEvent(
      mxEventId: String,
      room: Room,
      sender: User,
      membership: String,
      granterMember: User,
      parentsId: Set[UUID],
      authEventsId: Set[UUID]
  ) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_MEMBER,
        Some(granterMember.mxUserId),
        Some(MemberEventContent(membership = membership).asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId,
        streamOrder = sequence
      )
      .get
  }

  private def insertPLEvent(
      mxEventId: String,
      room: Room,
      sender: User,
      parentsId: Set[UUID],
      authEventsId: Set[UUID],
      plContent: PowerLevelEventContent
  ) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_POWER_LEVELS,
        Some(""),
        Some(plContent.asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId,
        streamOrder = sequence
      )
      .get
  }

  private def insertJoinRulesEvent(
      mxEventId: String,
      room: Room,
      sender: User,
      joinRule: String,
      parentsId: Set[UUID],
      authEventsId: Set[UUID]
  ) = {
    sequence += 1
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_JOIN_RULES,
        Some(""),
        Some(Map("join_rule" -> joinRule).asJson),
        mxEventId = s"$$$mxEventId",
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId,
        streamOrder = sequence
      )
      .get
  }

  "v2RoomImpl resolve algorithm" should {

    val alice  = authRepo.createUser("@alice:example.com").get
    val bob    = authRepo.createUser("@bob:example.com").get
    val carol  = authRepo.createUser("@carol:example.com").get
    val evelyn = authRepo.createUser("@evelyn:example.com").get

    "resolve direct graph" in {
      val testRoom = roomRepo.createRoom("!room1:example.com", "public", "5").get

      val createEvent = insertCreateEvent("createRoom", testRoom, alice)
      val aliceJoinEvent =
        insertMemberEvent("aliceJoin", testRoom, alice, "join", alice, Set(createEvent.id), Set(createEvent.id))
      val powerLevelEvent = insertPLEvent(
        "powerLevels",
        testRoom,
        alice,
        Set(aliceJoinEvent.id),
        Set(aliceJoinEvent.id),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100))
      )
      val joinRulesEvent =
        insertJoinRulesEvent(
          "joinRules",
          testRoom,
          alice,
          "private",
          Set(powerLevelEvent.id),
          Set(powerLevelEvent.id, aliceJoinEvent.id)
        )
      val inviteBob =
        insertMemberEvent(
          "aliceInvitesBob",
          testRoom,
          alice,
          "invite",
          bob,
          Set(joinRulesEvent.id),
          Set(aliceJoinEvent.id, powerLevelEvent.id)
        )
      val inviteCarol =
        insertMemberEvent(
          "aliceInvitesCarol",
          testRoom,
          alice,
          "invite",
          carol,
          Set(inviteBob.id),
          Set(aliceJoinEvent.id, powerLevelEvent.id)
        )
      val bobJoinEvent =
        insertMemberEvent(
          "bobJoin",
          testRoom,
          bob,
          "join",
          bob,
          Set(inviteCarol.id),
          Set(joinRulesEvent.id, inviteBob.id)
        )

      val algoImpl = new V2RoomImpl(testRoom)(config, dbContext, jedisPool)
      algoImpl.resolve(createEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 1)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }

      algoImpl
        .resolve(aliceJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 2)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(powerLevelEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 3)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(joinRulesEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 4)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(inviteBob) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 5)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(inviteBob)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(inviteCarol) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 6)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, carol.mxUserId) shouldBe Some(inviteCarol)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(inviteBob)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl
        .resolve(bobJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          assert(stateSet.size == 6)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, carol.mxUserId) shouldBe Some(inviteCarol)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(bobJoinEvent)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
    }

    "resolve ban vs power level" in {
      val testRoom = roomRepo.createRoom("!room2:example.com", "public", "5").get

      val createEvent = insertCreateEvent("CREATE", testRoom, alice)
      val aliceJoinEvent =
        insertMemberEvent("IMA", testRoom, alice, "join", alice, Set(createEvent.id), Set(createEvent.id))
      val powerLevelEvent = insertPLEvent(
        "IPOWER",
        testRoom,
        alice,
        Set(aliceJoinEvent.id),
        Set(createEvent.id, aliceJoinEvent.id),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100))
      )
      val joinRulesEvent =
        insertJoinRulesEvent(
          "IJR",
          testRoom,
          alice,
          "public",
          Set(powerLevelEvent.id),
          Set(createEvent.id, powerLevelEvent.id, aliceJoinEvent.id)
        )
      val bobJoinEvent =
        insertMemberEvent(
          "IMB",
          testRoom,
          bob,
          "join",
          bob,
          Set(joinRulesEvent.id),
          Set(createEvent.id, joinRulesEvent.id, powerLevelEvent.id)
        )
      val carolJoinEvent =
        insertMemberEvent(
          "IMC",
          testRoom,
          carol,
          "join",
          carol,
          Set(bobJoinEvent.id),
          Set(createEvent.id, joinRulesEvent.id, powerLevelEvent.id)
        )
      val plA = insertPLEvent(
        "PA",
        testRoom,
        alice,
        Set.empty,
        Set(createEvent.id, aliceJoinEvent.id, powerLevelEvent.id),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100, bob.mxUserId -> 50))
      )
      val plB = insertPLEvent(
        "PB",
        testRoom,
        alice,
        Set(carolJoinEvent.id),
        Set(createEvent.id, aliceJoinEvent.id, powerLevelEvent.id),
        PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(alice.mxUserId -> 100, bob.mxUserId -> 50))
      )

      val aliceBanEvelynEvent =
        insertMemberEvent(
          "MB",
          testRoom,
          alice,
          "ban",
          evelyn,
          Set(plA.id),
          Set(createEvent.id, aliceJoinEvent.id, plB.id)
        )
      val evelynJoin =
        insertMemberEvent(
          "IME",
          testRoom,
          evelyn,
          "join",
          evelyn,
          Set(aliceBanEvelynEvent.id),
          Set(createEvent.id, joinRulesEvent.id, plA.id)
        )

      val algoImpl = new V2RoomImpl(testRoom)(config, dbContext, jedisPool)
      algoImpl.resolve(createEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(aliceJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(powerLevelEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(joinRulesEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(bobJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(carolJoinEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(plA) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(plB) match {
        case (resolvedEvent, Success(stateSet)) =>
          //assert(resolvedEvent.rejected)
          //TODO : add test assertion when test case will be clarified
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
      algoImpl.resolve(aliceBanEvelynEvent) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(!resolvedEvent.rejected)
          //TODO : add test assertion when test case will be clarified
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }

      algoImpl
        .resolve(evelynJoin) match {
        case (resolvedEvent, Success(stateSet)) =>
          assert(resolvedEvent.rejected)
          //TODO : add test assertion when test case will be clarified
          stateSetCache.put(resolvedEvent.id, stateSet)
        case (_, Failure(f)) => fail(f)
      }
    }
  }
}
