mascarene {
  db {
    url="jdbc:postgresql://localhost:5432/postgres"
    url=${?POSTGRES_URL}
    datasource-class-name="org.postgresql.ds.PGSimpleDataSource"
    username="postgres"
    username=${?POSTGRES_USER}
    password="postgres"
    password=${?POSTGRES_PASSWORD}
  }
  redis {
    host="localhost"
    host=${?REDIS_HOST}
    port=6379
  }
  internal {
    default-ask-timeout = 30 seconds
    default-await-timeout = 30 seconds
  }
  server {
    domain-name = "localhost"
    base-url = "https://localhost/"
    bind-address = "localhost:8080"
    terminate-timeout = 30 seconds
    cache-ttl {
      event = 10 minutes
      state-set = 10 minutes
      user = 10 minutes
    }
    auth-session-cache = {
      expire-delay = 1 hour
      maximum-size = 10000
    }
    auth {
      hash {
        time-cost = 500 ms
        mem-cost = 65535
        parallelism = 1
        salt-length = 128
        hash-length = 128
      }
      token {
        expiration = 365 days
      }
    }
    secret {
      key = "CHANGE_ME"
    }
    room {
      worker-pool-size = 10
      resolution-queue-size = 5
      enqueue-wait-stash-buffer = 100
    }
  }

  matrix {
    default-room-version="5"
    supported-room-versions=["3", "4", "5"]
    interactive-auth = {
      register = {
        "flows": [
          {
            "stages": [ "example.type.foo", "example.type.bar" ]
          },
          {
            "stages": [ "example.type.foo", "example.type.baz" ]
          },
          {
            "stages": [ "m.login.dummy" ]
          }
        ],
        "params": {
          "example.type.baz": {
            "example_key": "foobar"
          }
        }
      }
      login = {}
    }
  }
}

akka {
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
  #loglevel="INFO"

  actor {
    provider = "cluster"
  }
  remote.artery {
    canonical {
      hostname = "127.0.0.1"
      port = 2551
    }
  }
  cluster {
    seed-nodes = ["akka://MascareneTEST@127.0.0.1:2551"]
    min-nr-of-members = 1
    //"akka://MascareneHS@127.0.0.1:2552"]
  }
  test {
    timefactor =  10.0
  }
}
akka.coordinated-shutdown.terminate-actor-system = off
akka.coordinated-shutdown.run-by-actor-system-terminate = off
akka.coordinated-shutdown.run-by-jvm-shutdown-hook = off
akka.cluster.run-coordinated-shutdown-when-down = off