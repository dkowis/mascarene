/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import com.typesafe.config.Config
import org.mascarene.homeserver.server.model.{DbContext, Event}
import redis.clients.jedis.JedisPool
import scalacache._
import scalacache.modes.try_._
import scalacache.redis._
import scalacache.serialization.circe._

import scala.jdk.DurationConverters._
import scala.util.Try

class StateSetCache(config: Config, dbContext: DbContext, jedisPool: JedisPool) {
  private[this] val stateSetCache: Cache[StateSet] =
    RedisCache[StateSet](jedisPool)

  private[this] val stateSetApi = new StateSetApi(config, dbContext)

  private def stateSetKey(eventId: UUID) = s"event(${eventId}).stateSet"

  private val stateSetCacheTtl = Some(config.getDuration("mascarene.server.cache-ttl.state-set").toScala)

  def getStateSet(event: Event): Try[Option[StateSet]] =
    stateSetCache.get(stateSetKey(event.id)).orElse {
      val stateSet = stateSetApi.loadStateSetForEvent(event)
      stateSet.foreach {
        case s if !s.isEmpty => stateSetCache.put(stateSetKey(event.id))(s, stateSetCacheTtl)
        case _               => ()
      }
      stateSet.map {
        case s if s.isEmpty => None
        case s              => Some(s)
      }
    }

  def put(eventId: UUID, stateSet: StateSet) = stateSetCache.put(stateSetKey(eventId))(stateSet, stateSetCacheTtl)
}
