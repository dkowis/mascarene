/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger
import org.mascarene.homeserver.server.model._
import org.mascarene.utils.LogUtils.time
import redis.clients.jedis.JedisPool

import scala.util.{Failure, Try}

object RoomStateResolver {
  trait Ack
  object ResolveAck extends Ack

  sealed trait Response
  case class StateResolved(updatedEvent: Event, stateSet: Try[StateSet]) extends Response

  sealed trait Command
  case class ResolveEvent(replyTo: ActorRef[StateResolved], event: Event) extends Command

  def apply(config: Config, dbContext: DbContext, jedisPool: JedisPool, room: Room): Behavior[Command] =
    Behaviors.setup { context =>
      val eventRepo = new EventRepo(dbContext)

      /**
        * Returns a factory method to create a room implementation class given a room version.
        * @param roomVersion room version used to create the implementation class
        * @return a function which can be used to create a room implementation object implementing RoomVersionImpl
        */
      def getRoomImplementationFactory(roomVersion: String): Try[Room => RoomVersionImpl] =
        Try {
          roomVersion match {
            case "1"                   => V1RoomImpl.apply(_)(config, dbContext, jedisPool)
            case "2" | "3" | "4" | "5" => V2RoomImpl.apply(_)(config, dbContext, jedisPool)
          }
        }

      // Actor creation will fail if the implementation can't be created
      val resolverImplementation = getRoomImplementationFactory(room.version).map { roomImplFactory =>
        roomImplFactory(room)
      }.get

      Behaviors.receiveMessage {
        case ResolveEvent(replyTo, event) =>
          context.log.debug(s"Resolving event $event")
          time(Logger(context.log), "event resolution time") {
            //Event resolution
            val (resolvedEvent, stateSetResult) = resolverImplementation.resolve(event)
            eventRepo.updateEventResolved(resolvedEvent.id, resolved = true)
            replyTo ! StateResolved(resolvedEvent.copy(resolved = true), stateSetResult)
          }
          Behaviors.same
        case x =>
          context.log.debug(s"received something else: $x")
          Behaviors.same
      }
    }
}
