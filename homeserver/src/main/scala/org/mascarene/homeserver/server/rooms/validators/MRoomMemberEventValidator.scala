/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import org.mascarene.homeserver.server.model.{DbContext, Event, EventRejection, EventRepo, EventTypes}
import org.mascarene.homeserver.server.rooms.StateSet
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.mascarene.sdk.matrix.core.ApiFailure
import cats.implicits._
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.model.rooms.ThirdPartyInviteContent
import redis.clients.jedis.JedisPool

import scala.jdk.DurationConverters._
import scala.concurrent.{Await, Future}
import scala.util.{Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

case class NoThirdPartyInviteEvent(event: Event) extends EventAuthValidation with LazyLogging {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some("NoThirdPartyInviteEvent: no m.room.third_party_invite event found in event state set")
    )
}

case class SignatureNotFound(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some("SignatureNotFound: No signature matching signatures in third party invite event")
    )
}

case class ThirdPartyInviteSenderMismatchInviteEventSender(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(
        "ThirdPartyInviteSenderMismatchInviteEventSender: no m.room.third_party_invite sender doesn't match invite event sender"
      )
    )
}

case class BadSenderMembership(event: Event, memberEvent: Event, expected: String) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(
        s"BadSenderMembership: sender doesn't have the expected '$expected' membership according to event ${memberEvent.mxEventId}"
      )
    )
}

case class BadTargetMembership(event: Event, memberEvent: Event, expected: String) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(
        s"BadTargetMembership: membership target user has an unexpected '$expected' membership according to event ${memberEvent.mxEventId}"
      )
    )
}

case class UnknownMembership(event: Event, membership: String) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"UnknownMembership: Unknown membership '$membership'"))
}

case class InsufficientPowerLevels(event: Event, senderMxId: String, power: String, userPl: Int, requiredPl: Int)
    extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(s"InsufficientPowerLevels: sender $senderMxId has insufficient '$power' power level: $userPl < $requiredPl")
    )
}

class MRoomMemberEventValidator(
    implicit
    config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool
) extends EventValidator
    with LazyLogging {

  private val defaultAwaitTimeout = config.getDuration("mascarene.internal.default-await-timeout").toScala
  private val eventRepo           = new EventRepo(dbContext)

  /**
    * Returns a user ban event if the stateset contains one for this user
    * @param userMxId
    * @param stateSet
    * @return
    */
  private def hasUserBannedEvent(userMxId: String, stateSet: StateSet): Try[Option[Event]] = {
    currentMembership(userMxId, stateSet).map {
      case Some((stateMemberEvent, memberContent)) =>
        if (memberContent.membership == "ban")
          Some(stateMemberEvent)
        else
          None
      case _ => None
    }
  }

  /**
    * Returns the current membership of an event
    * @param userMxId user to find membership
    * @param stateSet room state at event
    * @return a tuple containing the member event and its decoded content
    */
  private def currentMembership(userMxId: String, stateSet: StateSet): Try[Option[(Event, MemberEventContent)]] = {
    stateSet.getEvent(EventTypes.M_ROOM_MEMBER, userMxId) match {
      case None => Success(None)
      case Some(stateMemberEvent) =>
        val tryContent = for {
          eventContent <- eventRepo.getEventContent(stateMemberEvent)
          content <- eventContent.get.content.get
            .as[MemberEventContent]
            .toTry
        } yield content
        tryContent
          .map { memberContent => Some((stateMemberEvent, memberContent)) }
          .recoverWith {
            case f =>
              throw new ApiFailure(
                "ORG.MASCARENE.EVENT.BAD_CONTENT",
                s"membership event ${stateMemberEvent.mxEventId} content couldn't be decoded to MemberEventContent",
                None,
                f
              )
          }
    }
  }

  private def validateInviteMemberShip(
      event: Event,
      stateSet: StateSet,
      content: MemberEventContent
  ): ValidationResult[Event] = {
    validateInviteMemberShipContent(event, stateSet, content)
      .andThen(validateInviteMemberShipUsers(stateSet))
      .andThen(validateInviteMemberShipPL(stateSet))
  }

  /**
    * If the sender's power level is greater than or equal to the invite level, allow.
    * Otherwise, reject.
    *
    * @param stateSet
    * @param event
    * @return
    */
  private def validateInviteMemberShipPL(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
    val tryPl = for {
      sender <- authCache.getUser(event.senderId)
      userPl <- stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)
      pl     <- stateSetApi.getPowerLevelEventContent(stateSet)
    } yield (sender, userPl, pl)
    tryPl
      .map {
        case (Some(sender), Some(userPl), Some(statePl)) =>
          if (userPl >= statePl.invite)
            event.validNec
          else
            InsufficientPowerLevels(event, sender.mxUserId, "invite", userPl, statePl.invite).invalidNec
        case _ => InternalError(event, "Couldn't get/decode sender or target power level events").invalidNec
      }
      .recover(f => InternalError(event, s"Couldn't validate invite membership event: ${f.getMessage}").invalidNec)
      .get
  }

  private def validateInviteMemberShipUsers(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
    val senderMembershipFuture = Future.fromTry(
      authCache.getUser(event.senderId).flatMap(sender => currentMembership(sender.get.mxUserId, stateSet))
    )
    val targetUserMembershipFuture = Future.fromTry(currentMembership(event.stateKey.get, stateSet))
    val memberShipFuture = for {
      senderMembership     <- senderMembershipFuture
      targetUserMembership <- targetUserMembershipFuture
    } yield (senderMembership, targetUserMembership)
    val validationFuture = memberShipFuture.map {
      case (Some((senderMemberShipEvent, senderMemberShip)), Some((targetMemberShipEvent, targetMemberShip))) =>
        if (senderMemberShip.membership != "join")
          BadSenderMembership(event, senderMemberShipEvent, "join").invalidNec
        else {
          if (targetMemberShip.membership == "join" || targetMemberShip.membership == "ban")
            BadTargetMembership(event, targetMemberShipEvent, targetMemberShip.membership).invalidNec
          else
            event.validNec
        }
      case (Some(_), None) => event.validNec
      case _               => InternalError(event, "Couldn't get/decode sender or target membership events").invalidNec
    }
    Await.result(validationFuture, defaultAwaitTimeout)
  }

  private def validateInviteMemberShipContent(
      event: Event,
      stateSet: StateSet,
      content: MemberEventContent
  ): ValidationResult[Event] = {
    if (content.third_party_invite.isEmpty) {
      event.validNec
    } else {
      // If content has third_party_invite key
      val bannedTry = for {
        sender   <- authCache.getUser(event.senderId)
        banEvent <- hasUserBannedEvent(sender.get.mxUserId, stateSet)

      } yield (sender, banEvent)
      bannedTry.map {
        case (None, None) =>
          InternalError(event, s"Couldn't not get event sender or banned event (if it exists").invalidNec
        case (_, Some(banEvent)) => SenderIsBanned(event, banEvent).invalidNec //If target user is banned, reject.
        case (Some(sender), None) =>
          if (content.third_party_invite.get.signed.mxid != event.stateKey.get)
            ThirdPartyInviteSignatureMxIdMismatchStateKey(event).invalidNec //If mxid does not match state_key, reject.
          else {
            stateSet
              .getEvent(EventTypes.M_ROOM_THIRD_PARTY_INVITE, sender.mxUserId)
              .map { thirdPartyInviteEvent =>
                if (thirdPartyInviteEvent.senderId != event.senderId)
                  ThirdPartyInviteSenderMismatchInviteEventSender(event).invalidNec //If sender does not match sender of the m.room.third_party_invite, reject.
                else {
                  // If any signature in signed matches any public key in the m.room.third_party_invite event, allow.
                  // The public keys are in content of m.room.third_party_invite as:
                  //    A single public key in the public_key field.
                  //    A list of public keys in the public_keys field.
                  val thirdPartyInviteContentTry = for {
                    eventContent <- eventRepo.getEventContent(thirdPartyInviteEvent)
                    content      <- eventContent.get.content.get.as[ThirdPartyInviteContent].toTry
                  } yield content
                  thirdPartyInviteContentTry
                    .map { thirdPartyInviteContent =>
                      val signaturesKey: Seq[String] = content.third_party_invite.get.signed.signatures.keys.toSeq
                      val thirdPartySignaturesKey: Seq[String] = thirdPartyInviteContent.public_keys
                        .getOrElse(List.empty)
                        .map(_.public_key)
                        .appended(thirdPartyInviteContent.public_key)
                      if (thirdPartySignaturesKey.intersect(signaturesKey).isEmpty)
                        SignatureNotFound(event).invalidNec
                      else event.validNec
                    }
                    .recoverWith {
                      case f =>
                        Success(
                          InternalError(
                            event,
                            s"third party event  ${thirdPartyInviteEvent.mxEventId} content couldn't be decoded to ThirdPartyInviteContent: ${f.getMessage}"
                          ).invalidNec
                        )
                    }
                    .get
                }
              }
              .getOrElse(
                NoThirdPartyInviteEvent(event).invalidNec
              ) //If there is no m.room.third_party_invite event in the current room state with state_key matching token, reject.
          }
      }.get
    }
  }

  private def validateJoinMemberShip(
      event: Event,
      stateSet: StateSet
  ): ValidationResult[Event] = {
    validatePreviousEvent(event).andThen(validateSender(stateSet))
  }

  /**
    *If the only previous event is an m.room.create and the state_key is the creator, allow.
    * @param event
    * @return
    */
  private def validatePreviousEvent(event: Event): ValidationResult[Event] = {
    parentChain(event)
      .map { parents =>
        if (parents.size == 1 && parents.head.eventType != EventTypes.M_ROOM_CREATE)
          ParentEventMustBeUniqueAndCreate(event).invalidNec
        else event.validNec

      }
      .recover(f => InternalError(event, s"Couldn't get event's parents: ${f.getMessage}").invalidNec)
      .get
  }

  private def validateSender(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
    authCache
      .getUser(event.senderId)
      .flatMap { sender =>
        (sender, event.stateKey) match {
          case (Some(sender), Some(stateKey)) if sender.mxUserId == stateKey =>
            hasUserBannedEvent(sender.mxUserId, stateSet)
              .map {
                case Some(banEvent) => SenderIsBanned(event, banEvent).invalidNec
                case None           => event.validNec
              }
          case _ => Success(SenderMismatchStatekey(event).invalidNec)
        }
      }
      .recover(f =>
        InternalError(event, s"Couldn't not get event sender or ban event (if it exists): ${f.getMessage}").invalidNec
      )
      .get
  }

  private def validateLeaveMemberShip(
      event: Event,
      stateSet: StateSet
  ): ValidationResult[Event] = {
    val plFuture                   = Future.fromTry(stateSetApi.getPowerLevelEventContent(stateSet))
    val targetUserMembershipFuture = Future.fromTry(currentMembership(event.stateKey.get, stateSet))
    val targetPLFuture             = Future.fromTry(stateSetApi.userPowerLevel(event.stateKey.get, stateSet))
    val senderFuture               = Future.fromTry(authCache.getUser(event.senderId))
    val senderMembershipFuture =
      senderFuture.flatMap(sender => Future.fromTry(currentMembership(sender.get.mxUserId, stateSet)))
    val senderPLFuture =
      senderFuture.flatMap(sender => Future.fromTry(stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)))

    val memberShipFuture = for {
      sender           <- senderFuture
      senderMembership <- senderMembershipFuture
      senderPL         <- senderPLFuture
      targetMembership <- targetUserMembershipFuture
      targetPL         <- targetPLFuture
      pl               <- plFuture
    } yield (sender, senderMembership, senderPL, targetMembership, targetPL, pl)
    val validationFuture = memberShipFuture
      .map {
        case (
            Some(sender),
            Some((_, senderMembership)),
            Some(senderPl),
            Some((_, targetMembership)),
            Some(targetPl),
            Some(pl)
            ) =>
          if (sender.mxUserId == event.stateKey.get && (senderMembership.membership != "invite" || senderMembership.membership != "join")) {
            IncompatibleMembership(event, sender.mxUserId, senderMembership.membership).invalidNec
          } else if (senderMembership.membership != "join") {
            IncompatibleMembership(event, sender.mxUserId, senderMembership.membership).invalidNec
          } else if (targetMembership.membership == "ban" && senderPl < pl.ban) {
            InsufficientPowerLevels(event, sender.mxUserId, "ban", senderPl, pl.ban).invalidNec
          } else if (senderPl >= pl.kick && targetPl < senderPl) {
            event.validNec
          } else InsufficientPowerLevels(event, sender.mxUserId, "ban", senderPl, pl.ban).invalidNec
        case _ => InternalError(event, "Couldn't validate leave membership sender").invalidNec
      }
      .onError { f =>
        Future.successful(
          InternalError(event, s"Couldn't not validate membership event: ${f.getMessage}").invalidNec
        )
      }

    Await.result(validationFuture, defaultAwaitTimeout)
  }

  private def validateBanMemberShip(event: Event, stateSet: StateSet) = {

    val plFuture       = Future.fromTry(stateSetApi.getPowerLevelEventContent(stateSet))
    val targetPLFuture = Future.fromTry(stateSetApi.userPowerLevel(event.stateKey.get, stateSet))
    val senderFuture   = Future.fromTry(authCache.getUser(event.senderId))
    val senderMembershipFuture =
      senderFuture.flatMap(sender => Future.fromTry(currentMembership(sender.get.mxUserId, stateSet)))
    val senderPLFuture =
      senderFuture.flatMap(sender => Future.fromTry(stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)))

    val memberShipFuture = for {
      sender           <- senderFuture
      senderMembership <- senderMembershipFuture
      senderPL         <- senderPLFuture
      targetPL         <- targetPLFuture
      pl               <- plFuture
    } yield (sender, senderMembership, senderPL, targetPL, pl)
    val validationFuture = memberShipFuture
      .map {
        case (
            Some(sender),
            Some((_, senderMembership)),
            Some(senderPl),
            Some(targetPl),
            Some(pl)
            ) =>
          if (senderMembership.membership != "join") {
            IncompatibleMembership(event, sender.mxUserId, senderMembership.membership).invalidNec
          } else if (senderPl >= pl.ban && targetPl < senderPl) {
            event.validNec
          } else InsufficientPowerLevels(event, sender.mxUserId, "ban", senderPl, pl.ban).invalidNec
        case _ => InternalError(event, "Couldn't validate ban membership sender").invalidNec
      }
      .onError { f =>
        Future.successful(
          InternalError(event, s"Couldn't not validate membership event: ${f.getMessage}").invalidNec
        )
      }

    Await.result(validationFuture, defaultAwaitTimeout)

  }

  def validate(event: Event, stateSet: StateSet): ValidationResult[Event] =
    validateAuthEvents(event, stateSet).andThen { event =>
      val tryContent = for {
        eventContent <- eventRepo.getEventContent(event)
        content      <- eventContent.get.content.get.as[MemberEventContent].toTry
      } yield content
      tryContent
        .map { membershipContent =>
          membershipContent.membership match {
            case "join"   => validateJoinMemberShip(event, stateSet)
            case "invite" => validateInviteMemberShip(event, stateSet, membershipContent)
            case "leave"  => validateLeaveMemberShip(event, stateSet)
            case "ban"    => validateBanMemberShip(event, stateSet)
            case other    => UnknownMembership(event, other).invalidNec
          }
        }
        .recover { f =>
          logger.warn(s"Validation failed with cause: ${f.getMessage}", f)
          InternalError(event, s"Validation failed with cause: ${f.getMessage}").invalidNec
        }
        .get
    }
}
