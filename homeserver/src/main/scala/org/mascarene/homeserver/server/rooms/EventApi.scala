/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.server.model.{DbContext, Event, EventContent, EventRepo, User}

import scala.util.Try

class EventApi(
    val config: Config,
    dbContext: DbContext
) extends LazyLogging
    with ImplicitAskTimeOut {

  private val eventRepo = new EventRepo(dbContext)

  def getEventsContents(events: List[Event]): Try[Map[Event, EventContent]] =
    eventRepo.getEventsContent(events.map(_.id))

  def getEventsSenders(events: List[Event]): Try[Map[Event, User]] =
    eventRepo.getEventsSenders(events.map(_.id))
}
