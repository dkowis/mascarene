/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import org.mascarene.homeserver.server.model.{DbContext, Event, EventRejection, EventRepo}
import org.mascarene.homeserver.server.rooms.StateSet
import org.mascarene.utils.UserIdentifierUtils
import cats.implicits._
import com.typesafe.config.Config
import redis.clients.jedis.JedisPool

import scala.util.{Failure, Success, Try}

case class InvalidIdentifierFormat(message: String, event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"InvalidIdentifierFormat: $message"))
}

class MRoomAliasesEventValidator(
    implicit
    config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool
) extends EventValidator {
  private def validateSenderDomainMatchesStateKey(event: Event): ValidationResult[Event] = {
    val domainMatch = for {
      sender         <- authCache.getUser(event.senderId)
      userIdentifier <- UserIdentifierUtils.parse(sender.get.mxUserId)
      res <- Try {
        userIdentifier.domain == event.stateKey.get
      }
    } yield res
    domainMatch match {
      case Failure(f)     => InvalidIdentifierFormat(f.getMessage, event).invalidNec
      case Success(false) => SenderDomainMismatchStateKey(event).invalidNec
      case Success(true)  => event.validNec
    }
  }
  def validate(event: Event, stateSet: StateSet): ValidationResult[Event] =
    validateAuthEvents(event, stateSet).andThen(validateSenderDomainMatchesStateKey)

}
