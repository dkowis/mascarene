/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.server.model.{DbContext, Event}
import org.mascarene.homeserver.server.rooms.StateSet
import redis.clients.jedis.JedisPool
import cats.implicits._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.jdk.DurationConverters._
import scala.concurrent.{Await, Future}

class MRoomRedactionEventValidator(
    implicit
    config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool
) extends EventValidator
    with LazyLogging {

  private val defaultAwaitTimeout = config.getDuration("mascarene.internal.default-await-timeout").toScala

  def validate(event: Event, stateSet: StateSet): ValidationResult[Event] =
    validateAuthEvents(event, stateSet)
      .andThen(validateSenderHasJoined(stateSet))
      .andThen(validateRequiredPowerLevel(stateSet))
      .andThen(validateStateKeyFormat(stateSet))
      .andThen { event =>
        val currentPlContentFuture = Future.fromTry(stateSetApi.getPowerLevelEventContent(stateSet))
        val senderFuture           = Future.fromTry(authCache.getUser(event.senderId))
        val senderPLFuture =
          senderFuture.flatMap(sender => Future.fromTry(stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)))

        val future = for {
          sender           <- senderFuture
          senderPl         <- senderPLFuture
          currentPlContent <- currentPlContentFuture
        } yield (sender, senderPl, currentPlContent)
        val validationFuture = future
          .map {
            case (Some(sender), Some(senderPl), Some(currentPl)) =>
              if (senderPl >= currentPl.redact) event.validNec
              else
                InsufficientPowerLevels(event, sender.mxUserId, "redact", senderPl, currentPl.redact).invalidNec

            case other => InternalError(event, s"Couldn't check power level changes: unexpected $other").invalidNec
          }
          .recover(f => InternalError(event, s"Couldn't check power level changes: ${f.getMessage}").invalidNec)
        Await.result(validationFuture, defaultAwaitTimeout)
      }
}
