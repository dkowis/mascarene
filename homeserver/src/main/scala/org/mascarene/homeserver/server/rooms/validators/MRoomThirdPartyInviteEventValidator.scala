/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.server.model.{DbContext, Event}
import org.mascarene.homeserver.server.rooms.StateSet
import redis.clients.jedis.JedisPool
import cats.implicits._

class MRoomThirdPartyInviteEventValidator(
    implicit
    config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool
) extends EventValidator
    with LazyLogging {

  def validate(event: Event, stateSet: StateSet): ValidationResult[Event] =
    validateAuthEvents(event, stateSet).andThen(validateSenderHasJoined(stateSet)).andThen { event =>
      val tryPl = for {
        sender <- authCache.getUser(event.senderId)
        userPl <- stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)
        pl     <- stateSetApi.getPowerLevelEventContent(stateSet)
      } yield (sender, userPl, pl)
      tryPl
        .map {
          case (Some(sender), Some(userPl), Some(statePl)) =>
            if (userPl >= statePl.invite)
              event.validNec
            else
              InsufficientPowerLevels(event, sender.mxUserId, "invite", userPl, statePl.invite).invalidNec
          case _ => InternalError(event, "Couldn't get/decode sender or target power level events").invalidNec
        }
        .recover(f => InternalError(event, s"Couldn't validate third party event: ${f.getMessage}").invalidNec)
        .get
    }
}
