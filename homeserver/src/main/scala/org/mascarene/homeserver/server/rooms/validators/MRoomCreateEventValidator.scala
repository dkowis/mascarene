/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import org.mascarene.homeserver.server.model.{Event, EventRejection, Room}
import org.mascarene.homeserver.server.rooms.{AuthCache, EventCache, StateSet}
import org.mascarene.utils.{RoomIdentifierUtils, UserIdentifierUtils}
import cats.implicits._

import scala.util.{Failure, Success}

case class MRoomCreateHasNoParent(event: Event, parents: Set[Event]) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"MRoomCreateHasNoParent: event has one or more previous event: $parents"))
}

class MRoomCreateEventValidator(room: Room, eventCache: EventCache, authCache: AuthCache) {
  private def validateMRoomCreateEventHasNoParent(event: Event): ValidationResult[Event] = {
    eventCache
      .getEventParentEdges(event.id)
      .map { parents =>
        if (parents.nonEmpty)
          MRoomCreateHasNoParent(event, parents).invalidNec
        else
          event.validNec
      }
      .recover(f => InternalError(event, s"Couldn't not get event's parents: ${f.getMessage}").invalidNec)
      .get
  }

  private def validateRoomIdDomainMatchesSenderDomain(event: Event): ValidationResult[Event] = {
    val domainMatch = for {
      sender         <- authCache.getUser(event.senderId)
      userIdentifier <- UserIdentifierUtils.parse(sender.get.mxUserId)
      roomIdentifier <- RoomIdentifierUtils.parse(room.mxRoomId)
    } yield userIdentifier.domain == roomIdentifier.domain
    domainMatch match {
      case Failure(f)     => InvalidIdentifierFormat(f.getMessage, event).invalidNec
      case Success(false) => RoomDomainMismatchSenderDomain(event).invalidNec
      case Success(true)  => event.validNec
    }
  }

  def validate(event: Event, stateSet: StateSet): ValidationResult[Event] =
    validateMRoomCreateEventHasNoParent(event).andThen(validateRoomIdDomainMatchesSenderDomain)

}
