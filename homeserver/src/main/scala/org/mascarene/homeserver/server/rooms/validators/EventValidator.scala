/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import org.mascarene.homeserver.server.model.{DbContext, Event, EventRejection, EventTypes}
import org.mascarene.homeserver.server.rooms.{AuthCache, EventCache, PowerLevelsUtils, StateSet, StateSetApi}
import cats.data.{Validated, ValidatedNec}
import cats.implicits._
import com.typesafe.config.Config
import redis.clients.jedis.JedisPool

import scala.util.{Success, Try}

case class ThirdPartyInviteSignatureMxIdMismatchStateKey(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(
        s"ThirdPartyInviteSignatureMxIdMismatchStateKey: The third party invite signature mxId doesn't match event state key"
      )
    )
}
case class IncompatibleMembership(event: Event, senderMxId: String, currentMembership: String)
    extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(
        s"IncompatibleMembership: event membership is incompatible with $senderMxId current membership: $currentMembership"
      )
    )
}
case class InvalidStateKeyContent(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some("InvalidStateKeyContent: The state key start with '@' and doesn't match sender")
    )
}

class EventValidator(
    implicit
    config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool
) {
  protected[this] val authCache   = new AuthCache(config, dbContext, jedisPool)
  protected[this] val eventCache  = new EventCache(config, dbContext, jedisPool)
  protected[this] val stateSetApi = new StateSetApi(config, dbContext)

  protected def authChain(event: Event): Try[Set[Event]] = {
    for {
      authEvents <- eventCache.getEventAuthEdges(event.id)
      r <- Try {
        authEvents.flatMap(e => authChain(e).get)
      }
    } yield authEvents.union(r)
  }

  protected def parentChain(event: Event): Try[Set[Event]] = {
    for {
      parentsEvents <- eventCache.getEventParentEdges(event.id)
      r <- Try {
        parentsEvents.flatMap(e => parentChain(e).get)
      }
    } yield parentsEvents.union(r)
  }

  /**
    * Reject if event has auth_events that:
    *  - have duplicate entries for a given type and state_key pair
    *  - have entries whose type and state_key don't match those specified by the auth events selection algorithm
    *  described in the server specification.
    *
    * If event does not have a m.room.create in its auth_events, reject.
    *
    * @param e
    * @param stateSet
    * @return
    */
  protected def validateAuthEvents(e: Event, stateSet: StateSet): ValidationResult[Event] = {
    authChain(e)
      .map { authEvents =>
        val duplicates = authEvents.groupBy(event => (event.eventType, event.stateKey.get)).count(_._2.size > 1)
        if (duplicates > 0)
          DuplicateEntriesInAuthEvents(e).invalidNec
        else {
          val otherAuthTypes = authEvents
            .map(_.eventType)
            .diff(
              Set(
                EventTypes.M_ROOM_CREATE,
                EventTypes.M_ROOM_MEMBER,
                EventTypes.M_ROOM_POWER_LEVELS,
                EventTypes.M_ROOM_JOIN_RULES
              )
            )
          if (otherAuthTypes.isEmpty)
            e.validNec
          else
            InvalidEventTypeInAuthChain(e, otherAuthTypes.toString()).invalidNec
        }
      }
      .recover(f => InternalError(e, s"Couldn't get event auth chain: ${f.getMessage}").invalidNec)
      .get
  }

  protected def validateSenderHasJoined(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
    val memberShipTry = for {
      sender     <- authCache.getUser(event.senderId)
      memberShip <- stateSetApi.getMembershipEventContent(sender.get.mxUserId, stateSet)
    } yield (sender, memberShip)
    memberShipTry
      .map {
        case (Some(sender), Some(membershipContent)) if membershipContent.membership == "join" => event.validNec
        case (Some(sender), _)                                                                 => IncompatibleMembership(event, sender.mxUserId, "!join").invalidNec
        case other                                                                             => InternalError(event, s"Unexpected result from state set: $other").invalidNec
      }
      .recover(f => InternalError(event, s"Couldn't validate user joined membership: ${f.getMessage}").invalidNec)
      .get
  }

  protected def validateRequiredPowerLevel(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
    val plTry = for {
      sender <- authCache.getUser(event.senderId)
      userPl <- stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)
      pl     <- stateSetApi.getPowerLevelEventContent(stateSet)
    } yield (sender, userPl, pl)
    plTry
      .map {
        case (Some(sender), Some(userPl), Some(pl)) =>
          val requiredPl = PowerLevelsUtils.requiredPowerLevelForEventType(event.eventType, pl)
          if (requiredPl > userPl)
            InsufficientPowerLevels(event, sender.mxUserId, s" event type ${event.eventType}", userPl, requiredPl).invalidNec
          else event.validNec
        case (Some(_), None, None) => event.validNec //No power level in room
        case other                 => InternalError(event, s"Unexpected result from state set: $other").invalidNec
      }
      .recover(f => InternalError(event, s"Couldn't validate required power level: ${f.getMessage}").invalidNec)
      .get
  }

  protected def validateStateKeyFormat(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
    val senderTry = for {
      sender <- authCache.getUser(event.senderId)
    } yield (sender)
    senderTry
      .map {
        case Some(sender) =>
          if (event.stateKey.get.startsWith("@") && event.stateKey.get != sender.mxUserId)
            InvalidStateKeyContent(event).invalidNec
          else event.validNec
        case other => InternalError(event, s"Unexpected result from state set: $other").invalidNec
      }
      .recover(f => InternalError(event, s"Couldn't validate state key format: ${f.getMessage}").invalidNec)
      .get
  }

  /**
    * Default validation rules
    * @param event
    * @param stateSet
    * @return
    */
  def validateDefault(event: Event, stateSet: StateSet): ValidationResult[Event] =
    validateAuthEvents(event, stateSet)
      .andThen(validateSenderHasJoined(stateSet))
      .andThen(validateRequiredPowerLevel(stateSet))
      .andThen(validateStateKeyFormat(stateSet))

}
