/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.server.model.{
  DbContext,
  Event,
  EventRepo,
  EventTypes,
  Room,
  StateSetVersion,
  StateSetVersionRepo
}
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.mascarene.matrix.client.r0.model.rooms.PowerLevelEventContent
import redis.clients.jedis.JedisPool
import io.circe._
import io.circe.generic.auto._

import scala.concurrent.{Await, Future}
import scala.util.{Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.jdk.DurationConverters._

/**
  * API methods to manipulate a state set or using its content
  */
class StateSetApi(config: Config, dbContext: DbContext) extends LazyLogging {
  private[this] val eventRepo           = new EventRepo(dbContext)
  private[this] val stateSetVersionRepo = new StateSetVersionRepo(dbContext)

  private val defaultAwaitTimeout = config.getDuration("mascarene.internal.default-await-timeout").toScala

  /**
    * Load the m.room.power_levels event content of the given state set
    * @param stateSet
    * @return
    */
  def getPowerLevelEventContent(stateSet: StateSet): Try[Option[PowerLevelEventContent]] = {
    stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") match {
      case None => Success(None)
      case Some(powerLevelEvent) =>
        val tryContent = for {
          eventContent <- eventRepo.getEventContent(powerLevelEvent)
          content      <- Try { PowerLevelsUtils.getPlContent(eventContent.get) }
        } yield content
        tryContent.map { plContent => Some(plContent) }
    }
  }

  /**
    * Calculate the user power level from power level event
    * @param userMxId user to find power level for
    * @param stateSet room state at event
    * @return a may be integer
    */
  def userPowerLevel(userMxId: String, stateSet: StateSet): Try[Option[Int]] = {
    getPowerLevelEventContent(stateSet)
      .map(mayBePlContent => mayBePlContent.map(PowerLevelsUtils.findUserPowerLevel(userMxId, _)))
  }

  /**
    * Get the m.room.member event with user state key from the given state set
    * @param userMxId user matrix Id to used as state key to look for m.room.member event in statekey
    * @param stateSet state key to look for
    * @return
    */
  def getMembershipEventContent(userMxId: String, stateSet: StateSet): Try[Option[MemberEventContent]] = {
    stateSet.getEvent(EventTypes.M_ROOM_MEMBER, userMxId) match {
      case None => Success(None)
      case Some(memberEvent) =>
        val tryContent = for {
          eventContent <- eventRepo.getEventContent(memberEvent)
          content      <- eventContent.get.content.get.as[MemberEventContent].toTry
        } yield content
        tryContent.map { content => Some(content) }
    }
  }

  /**
    * Get a m.room.member event content
    * @param memberEvent event to get gontent
    * @return
    */
  def getMembershipEventContent(memberEvent: Event): Try[MemberEventContent] = {
    for {
      eventContent <- eventRepo.getEventContent(memberEvent)
      content      <- eventContent.get.content.get.as[MemberEventContent].toTry
    } yield content
  }

  /**
    * Store a new state version (contained in `newStateSet`) for the give `room`.
    * @param room room associated this the state set
    * @param newStateSet new state set to store
    * @param previousStateSet previous state set (needed for calculating changes diff)
    * @return
    */
  def createNewVersion(room: Room, newStateSet: StateSet, previousStateSet: StateSet): Try[Long] = {
    val diffStateSet = newStateSet.diff(previousStateSet)
    val diffEvents   = diffStateSet.map { (_, _, event) => event.id }.toSet
    stateSetVersionRepo.createVersion(room.id, diffEvents)
  }

  /**
    * Load a room stateSet version at the given event
    * @param event
    * @return
    */
  def loadStateSetForEvent(event: Event): Try[StateSet] = {
    for {
      stateEventsIds <- stateSetVersionRepo.getVersionStateEvents(event.roomId, event.statesetVersion.getOrElse(-1))
      stateEvents    <- eventRepo.getEventsByIds(stateEventsIds)
    } yield StateSet.fromIterable(stateEvents.toList.sortBy(_.streamOrder).map(e => (e.eventType, e.stateKey.get, e)))
  }

  /**
    * Load the room stateSet at the stream order
    * @param event
    * @return
    */
  def loadStateSetAtStreamOrder(streamOrder: Long): Try[Option[StateSet]] = {
    eventRepo.getEventAtStreamOrder(streamOrder).flatMap {
      case None => Success(None)
      case Some(event) =>
        val tryStateSet = for {
          stateEventsIds <- stateSetVersionRepo.getVersionStateEvents(event.roomId, event.statesetVersion.getOrElse(-1))
          stateEvents    <- eventRepo.getEventsByIds(stateEventsIds)
        } yield StateSet.fromIterable(stateEvents.map(e => (e.eventType, e.stateKey.get, e)))
        tryStateSet.map(Some(_))
    }
  }
}
