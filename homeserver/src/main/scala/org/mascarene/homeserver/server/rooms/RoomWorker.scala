/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import akka.actor.typed.{ActorSystem, Behavior}
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import com.typesafe.config.Config
import org.mascarene.homeserver.server.model.{AuthRepo, DbContext, Event, EventContent, EventRepo, EventTypes, Room}
import redis.clients.jedis.JedisPool
import com.typesafe.scalalogging.Logger
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import io.circe.generic.auto._
import org.mascarene.homeserver.server.rooms.RoomServer.Command
import org.mascarene.homeserver.server.rooms.RoomWorker.CastStateEventEffects

import scala.util.{Failure, Success, Try}

object RoomWorker {
  trait Ack
  object ResolveAck extends Ack

  sealed trait Response

  sealed trait Command
  case class CastStateEventEffects(roomState: RoomState, stateEvent: Event) extends Command

  def apply(config: Config, dbContext: DbContext, jedisPool: JedisPool): Behavior[Command] = Behaviors.setup {
    context => Behaviors.setup(context => new RoomWorker(context, config, dbContext, jedisPool))
  }

}

class RoomWorker(context: ActorContext[RoomWorker.Command], config: Config, dbContext: DbContext, jedisPool: JedisPool)
    extends AbstractBehavior[RoomWorker.Command](context) {

  implicit val system: ActorSystem[_] = context.system
  val eventRepo                       = new EventRepo(dbContext)
  val authRepo                        = new AuthRepo(dbContext)
  val authCache                       = new AuthCache(config, dbContext, jedisPool)
  val roomApi                         = new RoomApi(config, dbContext, system)

  override def onMessage(msg: RoomWorker.Command): Behavior[RoomWorker.Command] = {
    msg match {
      case CastStateEventEffects(roomState, stateEvent) =>
        eventRepo.getEventContent(stateEvent).foreach { eventContent =>
          stateEvent.eventType match {
            case EventTypes.M_ROOM_MEMBER =>
              eventContent.foreach(content => castRoomMemberEffects(roomState, stateEvent, content))
            case _ =>
              context.log.warn(s"No side effect for event type `${stateEvent.eventType}`")
          }
          eventRepo.updateEventProcessed(stateEvent.id, processed = true)
        }
        Behaviors.same
    }
  }

  private def castRoomMemberEffects(
      roomState: RoomState,
      roomMemberEvent: Event,
      eventContent: EventContent
  ): Unit = {
    val stateKey = roomMemberEvent.stateKey.get
    val contentTry = for {
      memberEventContent <- eventContent.content.get.as[MemberEventContent].toTry
      user <- authRepo.getUserByMxId(stateKey).flatMap {
        case None               => authRepo.createUser(stateKey)
        case Some(existingUser) => Success(existingUser)
      }
    } yield (memberEventContent, user)
    contentTry
      .flatMap {
        case (memberEventContent, user) =>
          roomApi.createOrUpdateMembership(roomState.room, user, roomMemberEvent, memberEventContent.membership)
      }
      .recover(f => context.log.warn(s"Failed to cast m.room.member effects: ${f.getMessage}"))
  }
}
