/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.time.LocalDateTime
import java.util.UUID

import io.circe.Json

import scala.util.Try

case class AccountData(
    id: UUID,
    accountId: UUID,
    roomId: Option[UUID],
    eventType: String,
    eventContent: Json,
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

class AccountDataRepo(val dbContext: DbContext) extends JsonCodec {
  import dbContext._

  private val accountDatas = quote(querySchema[AccountData]("account_data"))
  private val rooms        = quote(querySchema[Room]("Rooms"))

  def storeAccountData(accountId: UUID, eventType: String, eventContent: Json): Try[AccountData] =
    storeAccountData(accountId, None, eventType, eventContent)

  def storeAccountData(accountId: UUID, roomId: Option[UUID], eventType: String, eventContent: Json): Try[AccountData] =
    Try {
      val newRow = AccountData(UUID.randomUUID(), accountId, roomId, eventType, eventContent, LocalDateTime.now(), None)
      run {
        accountDatas.insert(lift(newRow))
      }
      newRow
    }

  def getAccountData(accountId: UUID, eventType: String): Try[Option[AccountData]] = Try {
    run(
      accountDatas
        .filter(_.accountId == lift(accountId))
        .filter(_.eventType == lift(eventType))
        .filter(_.roomId.isEmpty)
    ).headOption
  }

  def getAccountData(accountId: UUID): Try[List[AccountData]] = Try {
    run(
      accountDatas
        .filter(_.accountId == lift(accountId))
        .filter(_.roomId.isEmpty)
    )
  }

  def getAccountData(accountId: UUID, roomId: UUID, eventType: String): Try[Option[AccountData]] = Try {
    run(
      accountDatas
        .filter(_.accountId == lift(accountId))
        .filter(_.eventType == lift(eventType))
        .filter(_.roomId == lift(Some(roomId): Option[UUID]))
    ).headOption
  }

  def getAccountData(accountId: UUID, roomId: UUID): Try[List[AccountData]] = Try {
    run(
      accountDatas
        .filter(_.accountId == lift(accountId))
        .filter(_.roomId == lift(Some(roomId): Option[UUID]))
    )
  }

  def getRoomsAccountData(accountId: UUID): Try[Map[Room, AccountData]] = Try {
    run(
      accountDatas
        .filter(_.accountId == lift(accountId))
        .join(rooms)
        .on((ad, r) => ad.roomId.contains(r.id))
    ).map { case (ad, r) => r -> ad }.toMap
  }
}
