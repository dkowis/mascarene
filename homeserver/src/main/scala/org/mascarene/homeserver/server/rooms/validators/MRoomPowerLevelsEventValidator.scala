/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms.validators

import cats.data.Validated.Valid
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.server.model.{DbContext, Event, EventRejection, EventRepo}
import org.mascarene.homeserver.server.rooms.{PowerLevelsUtils, StateSet}
import cats.implicits._
import org.mascarene.matrix.client.r0.model.rooms.PowerLevelEventContent
import org.mascarene.utils.UserIdentifierUtils
import redis.clients.jedis.JedisPool

import scala.concurrent.ExecutionContext.Implicits.global
import scala.jdk.DurationConverters._
import scala.concurrent.{Await, Future}
import scala.util.Try

case class InvalidPowerLevelContent(event: Event) extends EventAuthValidation with LazyLogging {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some("InvalidPowerLevelContent: event content contains invalid power level definitions (see users keys)")
    )
}

class MRoomPowerLevelsEventValidator(
    implicit
    config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool
) extends EventValidator
    with LazyLogging {

  private val defaultAwaitTimeout = config.getDuration("mascarene.internal.default-await-timeout").toScala
  private val eventRepo           = new EventRepo(dbContext)

  private def validateContent(
      event: Event,
      stateSet: StateSet,
      powerLevelContent: PowerLevelEventContent
  ): ValidationResult[Event] = {
    if (powerLevelContent.users.keys.exists(eventType => UserIdentifierUtils.parse(eventType).isFailure)) {
      InvalidPowerLevelContent(event).invalidNec
    } else {
      event.validNec
    }
  }

  private def validatePowerLevelChanges(stateSet: StateSet, newPlContent: PowerLevelEventContent)(
      event: Event
  ): ValidationResult[Event] = {
    val currentPlContentFuture = Future.fromTry(stateSetApi.getPowerLevelEventContent(stateSet))
    val senderFuture           = Future.fromTry(authCache.getUser(event.senderId))
    val senderPLFuture =
      senderFuture.flatMap(sender => Future.fromTry(stateSetApi.userPowerLevel(sender.get.mxUserId, stateSet)))

    val future = for {
      sender           <- senderFuture
      senderPl         <- senderPLFuture
      currentPlContent <- currentPlContentFuture
    } yield (sender, senderPl, currentPlContent)

    val validationFuture = future
      .map {
        case (Some(_), _, None) => event.validNec //No PL in room
        case (Some(sender), Some(senderPl), Some(currentPl)) =>
          val checks: Map[String, (Option[Int], Option[Int])] = Map(
            "users_default"  -> (currentPl.users_default, newPlContent.users_default),
            "events_default" -> (Some(currentPl.events_default), Some(newPlContent.events_default)),
            "state_default"  -> (Some(currentPl.state_default), Some(newPlContent.state_default)),
            "ban"            -> (Some(currentPl.ban), Some(newPlContent.ban)),
            "redact"         -> (Some(currentPl.redact), Some(newPlContent.redact)),
            "kick"           -> (Some(currentPl.kick), Some(newPlContent.kick)),
            "invite"         -> (Some(currentPl.invite), Some(newPlContent.invite))
          )
          val ret = checks.map {
            case (powerKey, (currentPl, newPl)) =>
              if (currentPl != newPl) {
                val requiredPl = (currentPl ++ newPl).minOption
                if (requiredPl.isDefined && requiredPl.get > senderPl)
                  InsufficientPowerLevels(event, sender.mxUserId, powerKey, senderPl, requiredPl.get).invalidNec
                else
                  event.validNec
              } else
                event.validNec
          }
          ret.find(_.isInvalid).getOrElse(event.validNec)
        case other => InternalError(event, s"Couldn't check power level changes: unexpected $other").invalidNec
      }
      .recover(f => InternalError(event, s"Couldn't check power level changes: ${f.getMessage}").invalidNec)

    Await.result(validationFuture, defaultAwaitTimeout)
  }

  def validate(event: Event, stateSet: StateSet): ValidationResult[Event] = {
    validateAuthEvents(event, stateSet)
      .andThen(validateSenderHasJoined(stateSet))
      .andThen(validateRequiredPowerLevel(stateSet))
      .andThen(validateStateKeyFormat(stateSet))
      .andThen { event =>
        val tryContent = for {
          eventContent <- eventRepo.getEventContent(event)
          content <- Try {
            PowerLevelsUtils.getPlContent(eventContent.get)
          }
        } yield content
        tryContent
          .map { powerLevelContent =>
            validateContent(event, stateSet, powerLevelContent).andThen(
              validatePowerLevelChanges(stateSet, powerLevelContent)
            )
          }
          .recover { f =>
            logger.warn(s"Validation failed with cause: ${f.getMessage}", f)
            InternalError(event, s"Validation failed with cause: ${f.getMessage}").invalidNec
          }
          .get
      }
  }
}
