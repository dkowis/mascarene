/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.users

import org.mascarene.homeserver.server.model.{DbContext, Event, Room, RoomRepo, User}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class UserApi(dbContext: DbContext) {
  val roomRepo = new RoomRepo(dbContext)

  /**
    * Get a user room membership
    * @param user the user to find membership
    * @return a list of triplet containing for each membership : the membered room, the membership type
    *         ("join", "ban"...) and the state event stating this membership
    */
  def getRoomMemberships(user: User): Future[Iterable[(Room, String, Event)]] = Future.fromTry {
    roomRepo.getRoomMemberships(user.userId).map { it =>
      it.map { case (membership, room, event) => (room, membership.membership, event) }
    }
  }

  def joinedRoom(user: User): Future[Iterable[Room]] = getRoomMemberships(user).map { it =>
    it.filter { case (_, membership, _) => membership == "join" }.map(_._1)
  }

}
