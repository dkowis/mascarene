/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import java.io.{BufferedWriter, File, FileWriter, OutputStreamWriter}
import java.util.UUID

import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging
import io.getquill.{PostgresJdbcContext, SnakeCase}
import org.mascarene.homeserver.server.model.{AuthRepo, DbContext, Event, EventContent, EventRepo, RoomRepo}
import org.mascarene.homeserver.version.BuildInfo
import org.mascarene.utils.LogUtils.time
import scalax.collection.Graph
import scalax.collection.GraphPredef._
import scalax.collection.GraphEdge._
import scalax.collection.io.dot._
import implicits._
import io.circe.Json
import org.apache.commons.lang3.StringEscapeUtils
import scalax.collection.edge.LkDiEdge

import scala.util.{Failure, Success, Try}
import scopt.OParser

import scala.collection.mutable

case class ExporterConfig(
    roomMxId: String = "",
    outputFile: Option[File] = None,
    eventDAG: Boolean = true,
    authDAG: Boolean = true
)

object EventGraphExporter extends LazyLogging {
  def main(args: Array[String]): Unit = {
    logger.debug(s"Mascarene Event Graph exporter (${BuildInfo.version})")

    val status = for {
      config      <- time(logger, "Configuration load") { Try { ConfigFactory.load() } }
      (dbContext) <- initDbContext(config)
    } yield (config, dbContext)

    status.map {
      case (config, dbContext) =>
        OParser.parse(cmdLineParser(), args, ExporterConfig()).map { exporterConfig =>
          time(logger, "Export") { buildEventGraph(exporterConfig, dbContext) }
        }
    }
  }

  def buildEventGraph(config: ExporterConfig, dbContext: DbContext) = {
    val eventRepo = new EventRepo(dbContext)
    val roomRepo  = new RoomRepo(dbContext)
    val authRepo  = new AuthRepo(dbContext)
    val senderMap = mutable.HashMap[UUID, String]()

    roomRepo.getRoomByMxId(config.roomMxId).map {
      case Some(room) =>
        Try {
          val events      = eventRepo.getRoomEvents(room.id).get
          val parentEdges = events.flatMap(e => eventRepo.getParentEvents(e.id).get.map(p => LkDiEdge(e, p)("parent")))
          val authEdges   = events.flatMap(e => eventRepo.getAuthEvents(e.id).get.map(p => LkDiEdge(e, p)("auth")))
          logger.debug(s"Number of events to export: ${events.size}")
          logger.debug(s" - Number of parent edges: ${parentEdges.size}")
          logger.debug(s" - Number of auth edges: ${authEdges.size}")
          val graph = Graph.from(events, parentEdges ++ authEdges)

          val root = DotRootGraph(
            directed = true,
            id = Some(config.roomMxId),
            attrStmts = List(
              DotAttrStmt(
                Elem.node,
                List(
                  DotAttr("shape", "box"),
                  DotAttr("fontname", "Courrier"),
                  DotAttr("fontsize", "10.0")
                )
              )
            ),
            attrList = List(DotAttr("rankdir", """"BT""""))
          )

          def edgeTransformer(innerEdge: Graph[Event, LkDiEdge]#EdgeT): Option[(DotGraph, DotEdgeStmt)] =
            innerEdge.edge match {
              case LkDiEdge(source, target, label) =>
                label match {
                  case label: String =>
                    Some(
                      (
                        root,
                        DotEdgeStmt(
                          source.value.mxEventId,
                          target.value.mxEventId,
                          if (label.nonEmpty) {
                            List(
                              DotAttr(
                                "color",
                                if (label == "parent") "blue"
                                else "red"
                              )
                            )
                          } else Nil
                        )
                      )
                    )
                }
            }

          val dot = graph.toDot(
            root,
            edgeTransformer,
            cNodeTransformer = Some(_.value match {
              case e: Event =>
                val sender  = getSenderMxID(e)
                val content = getEventContentString(e)
                Some(
                  (
                    root,
                    DotNodeStmt(
                      e.mxEventId,
                      List(
                        DotAttr("eventType", e.eventType),
                        DotAttr(
                          "label",
                          s"""<sender=<B>${sender}</B><BR ALIGN="left"/>type=<B>${e.eventType}</B><BR ALIGN="left"/>stateKey=<B>"${e.stateKey
                            .getOrElse("None")}"</B><BR ALIGN="left"/>id=<B>${e.mxEventId}</B><BR ALIGN="left"/><FONT POINT-SIZE="8">content=${content}</FONT> >"""
                        )
                      )
                    )
                  )
                )
            })
          )

          val bw = new BufferedWriter(config.outputFile match {
            case Some(outputFile) => new FileWriter(outputFile)
            case None             => new OutputStreamWriter(System.out)
          })
          bw.write(dot)
          bw.close()
        }.recover(f => logger.error("Error while querying events", f))

      case None => logger.error(s"No room found with ID ${config.roomMxId}")
    }

    def getSenderMxID(event: Event): String = {
      if (senderMap.contains(event.senderId)) senderMap(event.senderId)
      else {
        val sender = authRepo.getUserById(event.senderId).get.get
        senderMap += (sender.userId -> sender.mxUserId)
        sender.mxUserId
      }
    }

    def getEventContentString(event: Event): String = {
      val res = eventRepo.getEventContent(event)
      StringEscapeUtils.escapeHtml4(
        res.map(c => c.get.content.getOrElse(Json.Null).noSpacesSortKeys).getOrElse("").replace("\n", "\\l")
      )
    }

  }

  def initDbContext(config: Config): Try[DbContext] = {
    for {
      dataSource <- MascareneMain.createDataSource(config)
    } yield (new PostgresJdbcContext(SnakeCase, dataSource))
  }

  def cmdLineParser() = {
    val builder = OParser.builder[ExporterConfig]
    import builder._
    OParser.sequence(
      programName("Mascarene EventGraphExporter"),
      head("EventGraphExporter", BuildInfo.version),
      opt[String]('r', "room").required
        .text("ID of the room to export event graph")
        .valueName("roomID")
        .action((r, config) => config.copy(roomMxId = r)),
      opt[File]('f', "output")
        .valueName("<file>")
        .action((f, config) => config.copy(outputFile = Some(f)))
        .text("required output file for graph data"),
      opt[Unit]('p', "eventDAG").action((_, config) => config.copy(authDAG = false)).text("export event graph only"),
      opt[Unit]('a', "authDAG").action((_, config) => config.copy(eventDAG = false)).text("export auth graph only"),
      help("help").text("prints this usage text")
    )
  }
}
