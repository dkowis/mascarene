/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import akka.actor.typed.{Behavior, PostStop}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.StatusCodes.Unauthorized
import akka.http.scaladsl.server.RejectionHandler
import akka.http.scaladsl.settings.ServerSettings
import com.typesafe.config.Config
import org.mascarene.homeserver.server.model.DbContext
import akka.http.scaladsl.server.Directives._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.matrix.ApiErrorRejection
import org.mascarene.homeserver.matrix.client.{AccountDataApiRoutes, ClientApiRoutes, PushRulesApiRoutes}
import org.mascarene.homeserver.matrix.client.auth.AuthApiRoutes
import org.mascarene.homeserver.matrix.client.filtering.FilteringApiRoutes
import org.mascarene.homeserver.matrix.client.rooms.RoomApiRoutes
import org.mascarene.homeserver.matrix.client.syncing.SyncingApiRoutes
import org.mascarene.homeserver.server.rooms.{EventStreamSequence, RoomCluster}
import redis.clients.jedis.JedisPool

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object MascareneServer extends FailFastCirceSupport with LazyLogging {
  sealed trait Message
  private final case class StartFailed(cause: Throwable)   extends Message
  private final case class Started(binding: ServerBinding) extends Message
  case object Stop                                         extends Message

  def apply(host: String, port: Int, config: Config, dbContext: DbContext, jedisPool: JedisPool): Behavior[Message] =
    Behaviors.setup { ctx =>
      // http doesn't know about akka typed so provide untyped system
      implicit val untypedSystem: akka.actor.ActorSystem = ctx.system.toClassic
      implicit val ec: ExecutionContextExecutor          = ctx.system.executionContext

      val eventStreamSequence  = ctx.spawn(EventStreamSequence(config, dbContext), "EventStreamSequence")
      val roomCluster          = ctx.spawn(RoomCluster(config, dbContext, jedisPool, eventStreamSequence), "RoomCluster")
      val clientApiRoutes      = new ClientApiRoutes(config)
      val authApiRoutes        = new AuthApiRoutes(config, dbContext, ctx.system)
      val accountDataApiRoutes = new AccountDataApiRoutes(config, dbContext, ctx.system)
      val pushRulesApiRoutes   = new PushRulesApiRoutes(config, dbContext, ctx.system)
      val filteringApiRoutes   = new FilteringApiRoutes(config, dbContext, ctx.system)
      val syncingApiRoutes     = new SyncingApiRoutes(config, dbContext, ctx.system)
      val roomApiRoutes        = new RoomApiRoutes(config, dbContext, roomCluster, ctx.system)
      val routes = cors() {
        clientApiRoutes.routes ~
          authApiRoutes.routes ~
          accountDataApiRoutes.routes ~
          pushRulesApiRoutes.routes ~
          filteringApiRoutes.routes ~
          syncingApiRoutes.routes ~
          roomApiRoutes.routes
      }

      val serverSettings = ServerSettings(config)
      implicit def apiRejectionHandler: RejectionHandler =
        corsRejectionHandler.withFallback(
          RejectionHandler
            .newBuilder()
            .handle {
              case rejection: ApiErrorRejection =>
                ctx.log.warn(s"Api rejection occurred: ${rejection.apiError}")
                complete(Unauthorized, rejection.apiError)
            }
            .result()
        )

      val bindingFuture = Http().bindAndHandle(routes, interface = host, port = port, settings = serverSettings)
      ctx.pipeToSelf(bindingFuture) {
        case Success(binding) => Started(binding)
        case Failure(ex)      => StartFailed(ex)
      }

      def running(binding: ServerBinding): Behavior[Message] =
        Behaviors
          .receiveMessagePartial[Message] {
            case Stop =>
              ctx.log.info(
                "Stopping Mascarene server http://{}:{}/",
                binding.localAddress.getHostString,
                binding.localAddress.getPort
              )
              Behaviors.stopped
          }
          .receiveSignal {
            case (_, PostStop) =>
              binding.unbind()
              Behaviors.same
          }

      def starting(wasStopped: Boolean): Behaviors.Receive[Message] =
        Behaviors.receiveMessage[Message] {
          case StartFailed(cause) =>
            throw new RuntimeException("Server failed to start", cause)
          case Started(binding) =>
            ctx.log.info(
              "Mascarene server started at http://{}:{}/",
              binding.localAddress.getHostString,
              binding.localAddress.getPort
            )
            if (wasStopped) ctx.self ! Stop
            running(binding)
          case Stop =>
            // we got a stop message but haven't completed starting yet,
            // we cannot stop until starting has completed
            starting(wasStopped = true)
        }

      starting(wasStopped = false)
    }
}
