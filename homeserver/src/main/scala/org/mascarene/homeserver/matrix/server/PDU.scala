/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server

import io.circe.Json

case class PDU(
    room_id: String,
    sender: String,
    origin: String,
    origin_server_ts: Long,
    `type`: String,
    state_key: Option[String],
    content: Json,
    prev_event: Set[String],
    depth: Int,
    auth_events: Set[String],
    redacts: Option[String],
    unsigned: Option[Map[String, Json]],
    hashes: Map[String, String],
    signatures: Map[String, Map[String, String]]
)
