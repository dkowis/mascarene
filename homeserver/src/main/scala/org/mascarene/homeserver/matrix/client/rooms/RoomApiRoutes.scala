/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.rooms

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.{Json, Printer}
import io.circe.generic.auto._
import org.mascarene.homeserver.server.model.{DbContext, Event}
import org.mascarene.matrix.client.r0.model.rooms.{CreateRoomRequest, CreateRoomResponse}
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.{ApiErrorRejection, ApiErrors}
import org.mascarene.homeserver.matrix.client.auth.{AuthApi, AuthDirectives}
import org.mascarene.homeserver.server.rooms.RoomServer.RoomStateResponse
import org.mascarene.homeserver.server.rooms.{RoomApi, RoomCluster, RoomServer}
import org.mascarene.matrix.client.r0.validation.rooms.{InvalidVisibilityValue, RoomsValidator, UnsupportedRoomVersion}
import org.mascarene.matrix.client.r0.validation.rooms.RoomsValidator.ValidationResult

import scala.jdk.CollectionConverters._
import scala.concurrent.Future
import akka.actor.typed.scaladsl.AskPattern._

class RoomApiRoutes(
    val config: Config,
    dbContext: DbContext,
    val roomCluster: ActorRef[RoomCluster.Command],
    implicit val system: ActorSystem[_]
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit private val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  implicit private val ec               = system.executionContext
  protected val authApi                 = new AuthApi(config, dbContext, system)
  protected val roomApi                 = new RoomApi(config, dbContext, system)

  private val defaultRoomVersion    = config.getString("mascarene.matrix.default-room-version")
  private val supportedRoomVersions = config.getStringList("mascarene.matrix.supported-room-versions").asScala.toList

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("createRoom") {
      post {
        requireAuth { credentials =>
          entity(as[CreateRoomRequest]) { request =>
            validateCreateRoomRequest(request)
              .map { validatedRequest =>
                // Start a new room server
                logger.info(s"API CALL - createRoom: $validatedRequest")
                val createFuture = for {
                  room <- Future.fromTry(
                    roomApi.createRoom(validatedRequest.visibility.get, validatedRequest.room_version)
                  )
                  serverRef <- roomCluster
                    .ask[RoomCluster.GotRoomServer](RoomCluster.GetRoomServer(room, _))
                    .map(_.serverRef)
                  _         <- roomApi.postCreateRoomEvents(serverRef, credentials.user, validatedRequest)
                  roomState <- serverRef.ask[RoomServer.RoomStateResponse](replyTo => RoomServer.GetRoomState(replyTo))
                } yield roomState
                onComplete(createFuture) { completed =>
                  completed
                    .map {
                      case RoomStateResponse(roomState) =>
                        complete(CreateRoomResponse(room_id = roomState.room.mxRoomId))
                    }
                    .recover { failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))) }
                    .get
                }
              }
              .leftMap { chain =>
                logger.debug(s"CreateRoomRequest validation failed with result: $chain")
                reject(ApiErrorRejection(chain.head.error))
              }
              .getOrElse(reject(ApiErrorRejection(ApiErrors.InternalError())))
          }
        }
      }
    } ~
    path( "rooms" / Segment / "send" / Segment / Segment) { (roomId, eventType, txnId) =>
      put {
        requireAuth { credentials =>
          entity(as[Option[Json]]) { content =>
            logger.info(s"API CALL - rooms/$roomId/send/$eventType/$txnId: $content")
            val roomIdTry = roomApi.getRoomByMxId(roomId).map(_.get)
            roomIdTry.map { room =>
              val postEventFuture = for {
                serverRef <- roomCluster
                  .ask[RoomCluster.GotRoomServer](RoomCluster.GetRoomServer(room, _))
                  .map(_.serverRef)
                event         <- roomApi.postEvent(serverRef, credentials.user, eventType, content)
              } yield event
              onComplete(postEventFuture) { completed =>
                completed.map { e => complete(Map("event_id" -> e.mxEventId))
                }
                  .recover { failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))) }
                  .get
              }
            }.recover { failure => reject(ApiErrorRejection(ApiErrors.Unrecognized(Some(s"Invid roomId or unknown room: $roomId")))) }
              .get
          }
        }
      }
    }
  }

  /**
    * Validate create room request fields and initialize defaults according spec if not provided
    * @param request
    * @return
    */
  private def validateCreateRoomRequest(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
    import cats.implicits._
    def fillRoomVersion(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
      request.room_version match {
        case Some(v) if supportedRoomVersions.contains(v) => request.validNec
        case None                                         => request.copy(room_version = Some(defaultRoomVersion)).validNec
        case _                                            => UnsupportedRoomVersion(supportedRoomVersions.map(v => s"'$v'").mkString_(",")).invalidNec
      }
    }
    def fillPreset(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
      request.preset match {
        case Some(preset) => request.validNec
        case None =>
          request.visibility match {
            case Some("public")  => request.copy(preset = Some("public_chat")).validNec
            case Some("private") => request.copy(preset = Some("private_chat")).validNec
            case _               => InvalidVisibilityValue.invalidNec
          }
      }
    }

    RoomsValidator.validate(request).andThen(fillPreset).andThen(fillRoomVersion)

  }
}
