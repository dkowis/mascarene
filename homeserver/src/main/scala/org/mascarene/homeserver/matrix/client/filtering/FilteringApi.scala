/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.filtering

import java.util.UUID

import com.typesafe.config.Config
import org.mascarene.homeserver.server.model.{Account, DbContext, Filter, FilteringRepo}
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition

import scala.util.Try
import io.circe._
import io.circe.generic.auto._
import io.circe.parser.decode
import org.mascarene.utils.Codecs

class FilteringApi(config: Config, dbContext: DbContext) {
  private val filteringRepo = new FilteringRepo(dbContext)

  def createFilter(account: Account, filterDefinition: FilterDefinition): Try[Filter] =
    filteringRepo.createFilter(account, filterDefinition)

  def getFilter(filterId: UUID): Try[Option[Filter]] = filteringRepo.getFilter(filterId)

  def parseOrGetFilterDefinition(filterParameter: String): Try[FilterDefinition] = {
    if (filterParameter.startsWith("{")) {
      decode[FilterDefinition](filterParameter).toTry
    } else {
      getFilter(UUID.fromString(Codecs.base64Decode(filterParameter)))
        .map(_.get.filterDefinition.getOrElse(FilterDefinition()))
    }
  }

}
