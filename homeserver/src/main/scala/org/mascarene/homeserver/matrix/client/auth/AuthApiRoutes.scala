/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.auth

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive1, Route}
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.server.model.DbContext
import org.mascarene.homeserver.matrix.ApiErrorRejection
import org.mascarene.matrix.client.r0.model.auth._
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.{ApiErrorRejection, ApiErrors}
import org.mascarene.sdk.matrix.core.{ApiError, ApiFailure}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}

class AuthApiRoutes(
    val config: Config,
    dbContext: DbContext,
    implicit val system: ActorSystem[_]
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut with LazyLogging{
  implicit private val printer: Printer             = Printer.noSpaces.copy(dropNullValues = true)
  implicit private val ec: ExecutionContextExecutor = system.executionContext

  protected val authApi = new AuthApi(config, dbContext, system)

  private def withRegistrationAuthFlow(endpoint: String): Directive1[RegisterRequest] =
    entity(as[RegisterRequest]).flatMap { req =>
      onComplete(authApi.checkAuthFlow(endpoint, req)).flatMap {
        case Success(Left(request))       => provide(request)
        case Success(Right(authFlow))     => complete(StatusCodes.Unauthorized, authFlow)
        case Failure(failure: ApiFailure) => reject(ApiErrorRejection(failure.toApiError))
        case Failure(failure) =>
          reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
      }
    }

  import pureconfig._
  import pureconfig.generic.auto._
  private val loginAuthFlow = Try {
    ConfigSource
      .fromConfig(config.getConfig("mascarene.matrix.interactive-auth.login"))
      .loadOrThrow[LoginAuthFlow]
  }

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("login") {
      get {
        loginAuthFlow match {
          case Failure(failure) => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
          case Success(flow)    => complete(flow)
        }
      } ~
        post {
          extractUserAgent { userAgent =>
            entity(as[LoginRequest]) {
              case req @ LoginRequest("m.login.password", identifier, password, _, deviceId, initialDisplayName)
                  if identifier.contains("type") && identifier.contains("user") =>
                logger.info(s"API CALL - POST login: $req")
                authApi.loginAuthenticate(
                  identifier("user"),
                  password.getOrElse(""),
                  deviceId,
                  initialDisplayName,
                  userAgent
                ) match {
                  case Success((_, user, device, authToken)) =>
                    complete(LoginResponse(user.mxUserId, authToken.encodedToken.get, device.mxDeviceId))
                  case Failure(failure: ApiFailure) =>
                    reject(ApiErrorRejection(failure.toApiError))
                  case Failure(failure) =>
                    reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                }
              case LoginRequest("m.login.password", _, _, _, _, _) =>
                complete(
                  StatusCodes.Unauthorized,
                  ApiError("M_FORBIDDEN", Some("The provided authentication data was incorrect."))
                )
              case LoginRequest(otherType, _, _, _, _, _) =>
                complete(StatusCodes.BadRequest, ApiError("M_UNKNOWN", Some(s"Unsupported login type $otherType")))
            }
          }
        }
    } ~
      path("logout") {
        requireAuth { credentials =>
          Future { authApi.logout(credentials.device.deviceId) }
          complete(())
        }
      } ~
      path("logout" / "all") {
        requireAuth { credentials =>
          Future { authApi.logoutAll(credentials.user.userId) }
          complete(())
        }
      } ~
      path("account" / "whoami") {
        requireAuth { credentials => complete(WhoAmIResponse(credentials.user.mxUserId)) }
      } ~
      path("register") {
        post {
          extractUserAgent { userAgent =>
            withRegistrationAuthFlow("register") { req =>
              logger.info(s"API CALL - POST register: $req")
              req.kind.getOrElse("user") match {
                case "user" | "guest" =>
                  authApi.registerUser(req, userAgent) match {
                    case Success(resp) => complete(resp)
                    case Failure(failure: ApiFailure) =>
                      reject(ApiErrorRejection(failure.toApiError))
                    case Failure(failure) =>
                      reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                  }
                case otherKind =>
                  reject(
                    ApiErrorRejection(
                      ApiErrors
                        .Unrecognized(Some(s"Unrecognized user kind '$otherKind'. Must be one of: ['guest', 'user']"))
                    )
                  )
              }
            }
          }
        }
      }
  }
}
