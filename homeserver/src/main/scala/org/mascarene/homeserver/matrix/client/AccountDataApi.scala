/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client

import java.util.UUID

import io.circe.Json
import org.mascarene.homeserver.server.model.{Account, AccountData, AccountDataRepo, DbContext, Room}

import scala.util.Try

class AccountDataApi(dbContext: DbContext) {
  private val accountDataRepo = new AccountDataRepo(dbContext)

  def storeAccountData(account: Account, roomId: UUID, dataType: String, value: Json): Try[AccountData] =
    accountDataRepo.storeAccountData(account.accountId, Some(roomId), dataType, eventContent = value)

  def storeAccountData(account: Account, dataType: String, value: Json): Try[AccountData] =
    accountDataRepo.storeAccountData(account.accountId, dataType, eventContent = value)

  def getAccountData(account: Account, room: Room, dataType: String): Try[Option[AccountData]] =
    accountDataRepo.getAccountData(account.accountId, room.id, dataType)

  def getAccountData(account: Account, room: Room): Try[List[AccountData]] =
    accountDataRepo.getAccountData(account.accountId, room.id)

  def getRoomsAccountData(account: Account): Try[Map[Room, Map[String, Json]]] =
    accountDataRepo.getRoomsAccountData(account.accountId).map { resultMap =>
      resultMap.groupBy(_._1).map { case (k, it) => k -> Map.from(it.map(t => t._2.eventType -> t._2.eventContent)) }

    }

  def getAccountData(account: Account, dataType: String): Try[Option[AccountData]] =
    accountDataRepo.getAccountData(account.accountId, dataType)

  def getAccountData(account: Account): Try[List[AccountData]] =
    accountDataRepo.getAccountData(account.accountId)
}
