/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix

import org.mascarene.sdk.matrix.core.ApiError

object ApiErrors {
  def InternalError(message: Option[String] = None): ApiError  = ApiError("ORG.MASCARENE.HS.INTERNAL_ERROR", message)
  def ForbiddenError(message: Option[String] = None): ApiError = ApiError("M_FORBIDDEN", message)
  def Unrecognized(message: Option[String] = None): ApiError   = ApiError("M_UNRECOGNIZED", message)
  def BadJson(message: Option[String] = None): ApiError        = ApiError("M_BAD_JSON", message)
}
