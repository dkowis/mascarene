/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.auth

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import com.github.benmanes.caffeine.cache.Caffeine
import com.typesafe.config.Config
import org.mascarene.matrix.client.r0.model.auth.AuthFlow
import org.mascarene.utils.Codecs
import scalacache._
import scalacache.caffeine._
import scalacache.modes.try_._

object AuthSessionCache {
  sealed trait Response
  sealed case class CacheStats(
      hitCount: Long,
      missCount: Long,
      loadSuccessCount: Long,
      loadFailureCount: Long,
      evictionCount: Long,
      evictionWeight: Long
  ) extends Response
  sealed case class AuthSessionStored(authFlow: AuthFlow)           extends Response
  sealed case class AuthSessionResponse(authFlow: Option[AuthFlow]) extends Response

  sealed trait Command
  final case class GetCacheStats(replyTo: ActorRef[CacheStats])                             extends Command
  final case class PutAuthSession(authFlow: AuthFlow, replyTo: ActorRef[AuthSessionStored]) extends Command
  final case class GetAuthSession(session: String, replyTo: ActorRef[AuthSessionResponse])  extends Command
  final case class UpdateAuthSession(authFlow: AuthFlow)                                    extends Command
  final case class InvalidateAuthSession(authFlow: AuthFlow)                                extends Command

  def apply(config: Config): Behavior[Command] = {
    Behaviors.setup(context => new AuthSessionCache(context, config))
  }
}

class AuthSessionCache(context: ActorContext[AuthSessionCache.Command], config: Config)
    extends AbstractBehavior[AuthSessionCache.Command](context) {
  import AuthSessionCache._

  import scala.jdk.DurationConverters._

  private val cacheExpireDelay = config.getDuration("mascarene.server.auth-session-cache.expire-delay")
  private val cacheMaximumSize = config.getLong("mascarene.server.auth-session-cache.maximum-size")
  private val underlyingCache = Caffeine
    .newBuilder()
    .recordStats()
    .expireAfterWrite(cacheExpireDelay)
    .maximumSize(cacheMaximumSize)
    .build[String, Entry[AuthFlow]]
  val sessionCache: Cache[AuthFlow] = CaffeineCache(underlyingCache)

  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case GetCacheStats(replyTo) =>
        val stats = underlyingCache.stats()
        replyTo ! CacheStats(
          stats.hitCount,
          stats.missCount,
          stats.loadSuccessCount,
          stats.loadFailureCount,
          stats.evictionCount,
          stats.evictionWeight
        )
        this
      case PutAuthSession(authFlow, replyTo) =>
        val session             = Codecs.genSessionId()
        val authFlowWithSession = authFlow.copy(session = Some(session))
        sessionCache.put(session)(authFlowWithSession)
        replyTo ! AuthSessionStored(authFlowWithSession)
        this
      case GetAuthSession(session, replyTo) =>
        replyTo ! AuthSessionResponse(sessionCache.get(session).getOrElse(None))
        this
      case UpdateAuthSession(authSession) =>
        authSession.session.foreach(session => sessionCache.put(session)(authSession))
        this
      case InvalidateAuthSession(authSession) =>
        authSession.session.foreach(session => sessionCache.remove(session))
        this
    }
  }
}
