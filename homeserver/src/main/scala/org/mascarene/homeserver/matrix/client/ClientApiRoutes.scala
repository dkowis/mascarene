/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._

final case class Version(versions: Seq[String], unstable_features: Option[Map[String, Boolean]] = None)

class ClientApiRoutes(val config: Config) extends FailFastCirceSupport {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  private val baseUrl = config.getString("mascarene.server.base-url")

  lazy val routes: Route = pathPrefix("_matrix" / "client") {
    path("versions") {
      get {
        complete(Version(Seq("r0.6.0")))
      }
    }
  } ~ path(".well-known" / "matrix" / "client") {
    get {
      complete(Map("m.homeserver" -> Map("base_url" -> baseUrl)))
    }
  }
}
