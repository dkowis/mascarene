/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
import sbt._

object Dependencies {
  object V {
    val circe         = "0.13.0"
    val circeOptics   = "0.13.0"
    val akkaHttp      = "10.1.11"
    val akka          = "2.6.5"
    val quillJdbc     = "3.5.1"
    val flyway        = "6.4.1"
    val postgresql    = "42.2.12"
    val playMailer    = "7.0.1"
    val scalaTest     = "3.1.1"
    val commonsCodec  = "1.11"
    val commonsLang   = "3.10"
    val argon2        = "2.7"
    val scalaGraph    = "1.13.2"
    val scalaGraphDot = "1.13.0"
    val slf4j         = "1.7.25"
    val pureConfig    = "0.12.3"
    val logback       = "1.2.3"
    val scalaLogging  = "3.9.2"
    val akkaHttpCirce = "1.32.0"
    val ipAddress     = "5.2.1"
    val uaparser      = "0.11.0"
    val jwt           = "4.3.0"
    val akkaCors      = "0.4.3"
    val scalaCache    = "0.28.0"
    val scopt         = "4.0.0-RC2"
  }

  val commonDependencies: Seq[ModuleID] = Seq(
    "com.typesafe.scala-logging" %% "scala-logging"            % V.scalaLogging,
    "ch.qos.logback"             % "logback-classic"           % V.logback,
    "com.typesafe.akka"          %% "akka-http"                % V.akkaHttp,
    "com.typesafe.akka"          %% "akka-stream"              % V.akka,
    "com.typesafe.akka"          %% "akka-slf4j"               % V.akka,
    "de.heikoseeberger"          %% "akka-http-circe"          % V.akkaHttpCirce,
    "io.circe"                   %% "circe-optics"             % V.circeOptics,
    "com.typesafe.akka"          %% "akka-http-testkit"        % V.akkaHttp % Test,
    "com.typesafe.akka"          %% "akka-stream-testkit"      % V.akka % Test,
    "com.typesafe.akka"          %% "akka-actor-testkit-typed" % V.akka % Test,
    "org.scalatest"              %% "scalatest"                % V.scalaTest % Test
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % V.circe)

  val sdkDependencies: Seq[ModuleID] = commonDependencies ++ Seq()

  val hsDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.typesafe.akka"     %% "akka-actor-typed"            % V.akka,
    "com.typesafe.akka"     %% "akka-stream-typed"           % V.akka,
    "com.typesafe.akka"     %% "akka-cluster-typed"          % V.akka,
    "com.typesafe.akka"     %% "akka-cluster-sharding-typed" % V.akka,
    "org.flywaydb"          % "flyway-core"                  % V.flyway,
    "io.getquill"           %% "quill-jdbc"                  % V.quillJdbc,
    "org.postgresql"        % "postgresql"                   % V.postgresql,
    "de.mkammerer"          % "argon2-jvm"                   % V.argon2,
    "org.scala-graph"       %% "graph-core"                  % V.scalaGraph,
    "org.scala-graph"       %% "graph-dot"                   % V.scalaGraphDot,
    "com.github.seancfoley" % "ipaddress"                    % V.ipAddress,
    "com.github.pureconfig" %% "pureconfig"                  % V.pureConfig,
    "com.github.cb372"      %% "scalacache-guava"            % V.scalaCache,
    "com.github.cb372"      %% "scalacache-redis"            % V.scalaCache,
    "com.github.cb372"      %% "scalacache-circe"            % V.scalaCache,
    "com.github.cb372"      %% "scalacache-caffeine"         % V.scalaCache,
    "com.pauldijou"         %% "jwt-circe"                   % V.jwt,
    "org.uaparser"          %% "uap-scala"                   % V.uaparser,
    "ch.megard"             %% "akka-http-cors"              % V.akkaCors,
    "org.apache.commons"    % "commons-lang3"                % V.commonsLang,
    "com.github.scopt"      %% "scopt"                       % V.scopt
  )

  val utilsDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.typesafe.akka" %% "akka-actor"   % V.akka,
    "commons-codec"     % "commons-codec" % V.commonsCodec,
    "com.typesafe.akka" %% "akka-testkit" % V.akka % Test
  )
}
