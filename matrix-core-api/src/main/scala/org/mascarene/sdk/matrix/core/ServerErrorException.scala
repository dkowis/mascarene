package org.mascarene.sdk.matrix.core

case class ServerErrorException(message: String, errcode: String, error: Option[String]) extends Throwable(message)
