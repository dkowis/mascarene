package org.mascarene.sdk.matrix.core

import akka.http.scaladsl.model.Uri
import io.circe.{Decoder, Encoder}
import org.mascarene.sdk.matrix.core.model.AuthToken

import scala.concurrent.Future

trait Transport {
  protected def doDelete[R](uri: Uri, queries: Map[String, String] = Map.empty)(
      implicit decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[R]
  protected def doGet[R](uri: Uri, queries: Map[String, String] = Map.empty)(
      implicit decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[R]
  protected def doPost[Q, R](uri: Uri, request: Q, queries: Map[String, String] = Map.empty)(
      implicit
      encoder: Encoder[Q],
      decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[R]
  protected def doPut[Q, R](uri: Uri, request: Q, queries: Map[String, String] = Map.empty)(
      implicit
      encoder: Encoder[Q],
      decoder: Decoder[R],
      token: Option[AuthToken] = None
  ): Future[R]
}
