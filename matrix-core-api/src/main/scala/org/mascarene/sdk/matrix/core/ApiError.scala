package org.mascarene.sdk.matrix.core

case class ApiError(errcode: String, error: Option[String] = None)
