/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.model

package object auth {
  trait HasAuthData {
    def auth: Option[Map[String, String]]
  }
  case class Flow(stages: List[String] = List.empty)
  case class AuthFlow(
      completed: Option[List[String]] = None,
      errcode: Option[String] = None,
      error: Option[String] = None,
      flows: List[Flow] = List.empty,
      params: Map[String, Map[String, String]] = Map.empty,
      session: Option[String] = None
  )

  case class LoginAuthFlow(flows: List[LoginFlow] = List.empty)
  case class LoginFlow(`type`: String)

  case class AuthData(`type`: String, session: Option[String] = None, example_credential: Option[String] = None)
  case class RegisterRequest(
      kind: Option[String] = Some("user"),
      auth: Option[Map[String, String]] = None,
      username: Option[String] = None,
      password: Option[String] = None,
      device_id: Option[String] = None,
      initial_device_display_name: Option[String] = None,
      inibit_login: Option[Boolean] = Some(false)
  ) extends HasAuthData

  case class RegisterResponse(user_id: String, access_token: Option[String], device_id: String)

  case class RequestTokenRequest(id_server: Option[String], client_secret: String, email: String, send_attempt: Int)

  case class PasswordRequest(new_password: String, auth: Map[String, String])

  case class DeactivateRequest(auth: Map[String, String])

  case class ThreePidIdentifier(medium: String, address: String)
  case class GetThreePidsResponse(threepids: List[ThreePidIdentifier])

  case class PostThreePidRequest(three_pid_creds: ThreePidCredentials, bind: Boolean)
  case class ThreePidCredentials(client_secret: String, id_server: String, sid: String)

  case class WhoAmIResponse(user_id: String)

  case class LoginRequest(
      `type`: String,
      identifier: Map[String, String] = Map.empty,
      password: Option[String] = None,
      token: Option[String] = None,
      device_id: Option[String] = None,
      initial_device_display_name: Option[String] = None
  )

  case class LoginResponse(
      user_id: String,
      access_token: String,
      device_id: String,
      well_known: Option[Map[String, String]] = None
  )
}
