package org.mascarene.matrix.client.r0.model

import io.circe.Json

package object pushrules {
  case class GetPushRulesResponse(global: RuleSet)
  case class RuleSet(
      content: List[PushRule] = List.empty,
      `override`: List[PushRule] = List.empty,
      room: List[PushRule] = List.empty,
      sender: List[PushRule] = List.empty,
      underride: List[PushRule] = List.empty
  )
  case class PushRule(
      actions: Json,
      default: Boolean,
      enabled: Boolean,
      rule_id: String,
      conditions: List[PushCondition],
      pattern: Option[String]
  )
  case class PushCondition(kind: String, key: Option[String], pattern: Option[String], is: Option[String])
}
