/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import org.mascarene.sdk.matrix.core.model.AuthToken
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.model.profiles._
import org.mascarene.sdk.matrix.core.Transport

import scala.concurrent.{ExecutionContextExecutor, Future}

trait ProfilesApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def setDisplayName(userId: String, displayName: String)(implicit token: AuthToken): Future[Unit] =
    doPut[SetDisplayNameRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/displayname"),
      SetDisplayNameRequest(Some(displayName))
    )

  def getDisplayName(userId: String)(implicit token: AuthToken): Future[GetDisplayNameResponse] =
    doGet[GetDisplayNameResponse](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/displayname"))

  def setAvatarUrl(userId: String, avatarUrl: String)(implicit token: AuthToken): Future[Unit] =
    doPut[SetAvatarUrlRequest, Unit](
      apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/avatar_url"),
      SetAvatarUrlRequest(Some(avatarUrl))
    )

  def getAvatarUrl(userId: String)(implicit token: AuthToken): Future[GetAvatarUrlResponse] =
    doGet[GetAvatarUrlResponse](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/avatar_url"))

  def getProfile(userId: String)(implicit token: AuthToken): Future[GetProfileResponse] =
    doGet[GetProfileResponse](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId"))
}
