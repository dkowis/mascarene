/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.circe.Json
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.model.events._
import org.mascarene.sdk.matrix.core.Transport
import org.mascarene.sdk.matrix.core.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}

trait EventsApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def sync(
      filter: Option[String] = None,
      since: Option[String] = None,
      full_state: Option[Boolean] = None,
      set_presence: Option[String] = None,
      timeout: Option[Int] = None
  )(implicit token: AuthToken): Future[SyncResponse] = {
    val query = List(
      filter.map(v => "filter"             -> v),
      since.map(v => "since"               -> v),
      full_state.map(v => "full_state"     -> v.toString),
      set_presence.map(v => "set_presence" -> v),
      timeout.map(v => "timeout"           -> v.toString)
    ).filter(_.isDefined).map(_.get).toMap

    doGet[SyncResponse](apiRoot.withPath(apiRoot.path + s"/r0/sync"), query)
  }

  def getRoomStateEvent(roomId: String, eventType: String, stateKey: String)(implicit token: AuthToken): Future[Json] =
    doGet[Json](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/state/$eventType/$stateKey"))

  def getRoomStateEvent(roomId: String, eventType: String)(implicit token: AuthToken): Future[Json] =
    doGet[Json](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/state/$eventType"))

  def getRoomStateEvent(roomId: String)(implicit token: AuthToken): Future[List[StateEvent]] =
    doGet[List[StateEvent]](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/state"))

  def getRoomMembers(roomId: String)(implicit token: AuthToken): Future[RoomMembersResponse] =
    doGet[RoomMembersResponse](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/members"))

  def getRoomJoinedMembers(roomId: String)(implicit token: AuthToken): Future[List[JoinedMembersResponse]] =
    doGet[List[JoinedMembersResponse]](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/joined_members"))

  def getRoomMessages(
      roomId: String,
      from: String,
      dir: String,
      to: Option[String] = None,
      limit: Option[Integer],
      filter: Option[String]
  )(implicit token: AuthToken): Future[List[RoomMessagesResponse]] = {
    val query = List(
      to.map(v => "to"          -> v),
      limit.map(v => "limit"    -> v.toString),
      filter.map(v => "timeout" -> v)
    ).filter(_.isDefined).map(_.get).toMap

    doGet[List[RoomMessagesResponse]](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/messages"), query)
  }

  def sendRoomEvent(roomId: String, eventType: String, stateKey: String, event: Json)(
      implicit token: AuthToken
  ): Future[SendRoomEventResponse] =
    doPut[Unit, SendRoomEventResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/state/$eventType/$stateKey"),
      ()
    )

  def sendRoomEvent(roomId: String, eventType: String, event: Json)(
      implicit token: AuthToken
  ): Future[SendRoomEventResponse] =
    doPut[Json, SendRoomEventResponse](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/state/$eventType"), event)

  def sendRoomEventWithTransaction(roomId: String, eventType: String, txnId: String, event: Json)(
      implicit token: AuthToken
  ): Future[SendRoomEventResponse] =
    doPut[Json, SendRoomEventResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/send/$eventType/$txnId"),
      event
    )

  def redactEvent(roomId: String, eventId: String, txnId: String, reason: String)(
      implicit token: AuthToken
  ): Future[RedactEventResponse] =
    doPut[RedactEventRequest, RedactEventResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/redact/$eventId/$txnId"),
      RedactEventRequest(reason)
    )
}
