/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0

import com.typesafe.scalalogging.LazyLogging
import io.circe.Decoder
import io.circe.parser.decode
import org.mascarene.sdk.matrix.core.model.ServerError
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser._
import org.mascarene.matrix.client.r0.model.auth.AuthFlow
import org.mascarene.sdk.matrix.core.{ApiFailure, ServerErrorException}

trait ClientResponseDecodeHandler extends LazyLogging {

  /**
    * Decode a server response, either to an object containing the server response (Right)
    * or to an object containing the server error response (Left)
    * @param entity
    * @param decoder
    * @tparam R
    * @return
    */
  protected def responseDecodeHandler[R](entity: String)(implicit decoder: Decoder[R]): R = {
    import cats.syntax.show._
    decode[R](entity) match {
      case Left(error) =>
        logger.debug(s"response can't be decoded to entity : '${error.show}'")
        //{"next_batch": "s11_65_4_1_1_1_1_17_1", "device_one_time_keys_count": {}, "account_data": {"events": [{"content": {"device": {}, "global": {"content": [{"default": true, "pattern": "toto", "enabled": true, "rule_id": ".m.rule.contains_user_name", "actions": ["notify", {"set_tweak": "sound", "value": "default"}, {"set_tweak": "highlight"}]}], "override": [{"default": true, "enabled": false, "conditions": [], "rule_id": ".m.rule.master", "actions": ["dont_notify"]}, {"default": true, "enabled": true, "conditions": [{"pattern": "m.notice", "kind": "event_match", "key": "content.msgtype"}], "rule_id": ".m.rule.suppress_notices", "actions": ["dont_notify"]}, {"default": true, "enabled": true, "conditions": [{"pattern": "m.room.member", "kind": "event_match", "key": "type"}, {"pattern": "invite", "kind": "event_match", "key": "content.membership"}, {"pattern": "@toto:localhost", "kind": "event_match", "key": "state_key"}], "rule_id": ".m.rule.invite_for_me", "actions": ["notify", {"set_tweak": "sound", "value": "default"}, {"set_tweak": "highlight", "value": false}]}, {"default": true, "enabled": true, "conditions": [{"pattern": "m.room.member", "kind": "event_match", "key": "type"}], "rule_id": ".m.rule.member_event", "actions": ["dont_notify"]}, {"default": true, "enabled": true, "conditions": [{"kind": "contains_display_name"}], "rule_id": ".m.rule.contains_display_name", "actions": ["notify", {"set_tweak": "sound", "value": "default"}, {"set_tweak": "highlight"}]}, {"default": true, "enabled": true, "conditions": [{"pattern": "@room", "kind": "event_match", "key": "content.body"}, {"kind": "sender_notification_permission", "key": "room"}], "rule_id": ".m.rule.roomnotif", "actions": ["notify", {"set_tweak": "highlight", "value": true}]}], "sender": [], "room": [], "underride": [{"default": true, "enabled": true, "conditions": [{"pattern": "m.call.invite", "kind": "event_match", "key": "type"}], "rule_id": ".m.rule.call", "actions": ["notify", {"set_tweak": "sound", "value": "ring"}, {"set_tweak": "highlight", "value": false}]}, {"default": true, "enabled": true, "conditions": [{"kind": "room_member_count", "is": "2"}, {"pattern": "m.room.message", "kind": "event_match", "key": "type"}], "rule_id": ".m.rule.room_one_to_one", "actions": ["notify", {"set_tweak": "sound", "value": "default"}, {"set_tweak": "highlight", "value": false}]}, {"default": true, "enabled": true, "conditions": [{"kind": "room_member_count", "is": "2"}, {"pattern": "m.room.encrypted", "kind": "event_match", "key": "type"}], "rule_id": ".m.rule.encrypted_room_one_to_one", "actions": ["notify", {"set_tweak": "sound", "value": "default"}, {"set_tweak": "highlight", "value": false}]}, {"default": true, "enabled": true, "conditions": [{"pattern": "m.room.message", "kind": "event_match", "key": "type"}], "rule_id": ".m.rule.message", "actions": ["notify", {"set_tweak": "highlight", "value": false}]}, {"default": true, "enabled": true, "conditions": [{"pattern": "m.room.encrypted", "kind": "event_match", "key": "type"}], "rule_id": ".m.rule.encrypted", "actions": ["notify", {"set_tweak": "highlight", "value": false}]}]}}, "type": "m.push_rules"}]}, "to_device": {"events": []}, "groups": {"leave": {}, "join": {}, "invite": {}}, "presence": {"events": [{"content": {"currently_active": true, "last_active_ago": 10, "presence": "online"}, "type": "m.presence", "sender": "@toto:localhost"}]}, "device_lists": {"changed": [], "left": []}, "rooms": {"leave": {}, "join": {"!kZIhOnkasTcssritaG:localhost": {"unread_notifications": {}, "timeline": {"limited": false, "prev_batch": "s11_65_4_1_1_1_1_17_1", "events": [{"origin_server_ts": 1525463895348, "sender": "@toto:localhost", "event_id": "$15254638950tIxlj:localhost", "unsigned": {"age": 1992484}, "state_key": "", "content": {"creator": "@toto:localhost"}, "type": "m.room.create"}, {"origin_server_ts": 1525463895379, "sender": "@toto:localhost", "event_id": "$15254638951ynDyx:localhost", "unsigned": {"age": 1992453}, "state_key": "@toto:localhost", "content": {"membership": "join", "avatar_url": null, "displayname": "toto"}, "membership": "join", "type": "m.room.member"}, {"origin_server_ts": 1525463895417, "sender": "@toto:localhost", "event_id": "$15254638952inmbu:localhost", "unsigned": {"age": 1992415}, "state_key": "", "content": {"redact": 50, "events_default": 0, "users": {"@toto:localhost": 100}, "invite": 0, "ban": 50, "state_default": 50, "events": {"m.room.avatar": 50, "m.room.history_visibility": 100, "m.room.canonical_alias": 50, "m.room.name": 50, "m.room.power_levels": 100}, "kick": 50, "users_default": 0}, "type": "m.room.power_levels"}, {"origin_server_ts": 1525463895488, "sender": "@toto:localhost", "event_id": "$15254638953GAECy:localhost", "unsigned": {"age": 1992344}, "state_key": "", "content": {"alias": "#testRoom:localhost"}, "type": "m.room.canonical_alias"}, {"origin_server_ts": 1525463895556, "sender": "@toto:localhost", "event_id": "$15254638954qpwEk:localhost", "unsigned": {"age": 1992276}, "state_key": "", "content": {"join_rule": "invite"}, "type": "m.room.join_rules"}, {"origin_server_ts": 1525463895597, "sender": "@toto:localhost", "event_id": "$15254638955EYNjd:localhost", "unsigned": {"age": 1992235}, "state_key": "", "content": {"history_visibility": "shared"}, "type": "m.room.history_visibility"}, {"origin_server_ts": 1525463895653, "sender": "@toto:localhost", "event_id": "$15254638956opCNB:localhost", "unsigned": {"age": 1992179}, "state_key": "", "content": {"guest_access": "can_join"}, "type": "m.room.guest_access"}, {"origin_server_ts": 1525463895689, "sender": "@toto:localhost", "event_id": "$15254638957vszhN:localhost", "unsigned": {"age": 1992143}, "state_key": "localhost", "content": {"aliases": ["#testRoom:localhost"]}, "type": "m.room.aliases"}, {"origin_server_ts": 1525463978823, "sender": "@toto:localhost", "event_id": "$15254639788mBlsA:localhost", "unsigned": {"age": 1909009}, "content": {"body": "test message", "msgtype": "m.text"}, "type": "m.room.message"}, {"origin_server_ts": 1525464428368, "sender": "@toto:localhost", "event_id": "$15254644289qQuqt:localhost", "unsigned": {"age": 1459464}, "content": {"body": "pipo", "msgtype": "m.text"}, "type": "m.room.message"}]}, "state": {"events": []}, "ephemeral": {"events": [{"content": {"user_ids": []}, "type": "m.typing"}]}, "account_data": {"events": []}}}, "invite": {}}}
        decode[AuthFlow](entity) match {
          case Left(error) =>
            logger.debug(s"response can't be decoded to AuthFlow: '${error.show}'")
            decode[ServerError](entity) match {
              case Left(error) =>
                logger.debug(s"Unexpected response: $entity")
                logger.warn(s"Unexpected response (JSON decoding failed): '${error.show}'")
                throw new ApiFailure(
                  "Failed to decode server response",
                  "IO.HBMC.DECODE_ERROR",
                  Some(error.toString + entity)
                )
              //ApiResponse(clientError = Some(ClientError("IO.HBMC.DECODE_ERROR", Some(error.toString + entity))))
              case Right(servererror) =>
                logger.debug(s"Server error response: $servererror")
                throw new ServerErrorException(s"$servererror", servererror.errcode, servererror.error)
              //ApiResponse(serverError = Some(servererror))
            }

          case Right(flow) =>
            logger.debug(s"Auth flow response: $flow")
            throw new AuthFlowException(
              s"$flow",
              flow.completed,
              flow.errcode,
              flow.error,
              flow.flows,
              flow.params,
              flow.session
            )
          //ApiResponse(authFlows = Some(flow))
        }
      case Right(t) => t
    }
  }

}
